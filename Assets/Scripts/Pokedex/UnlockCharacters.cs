﻿using UnityEngine;
using System.Collections;

public class UnlockCharacters : MonoBehaviour {
	
	private int screws;
	private int nuts;

	public int numScrews;
	public int numNuts;

	public bool locked = true;

	public GameObject lockedPanel;

	void Start(){
		screws = GameObject.Find("Manager").GetComponent<EarnManager>().screws;
		nuts = GameObject.Find("Manager").GetComponent<EarnManager>().nuts;

		locked = GameObject.Find("Manager").GetComponent<EarnManager>().locked;
		lockedPanel.gameObject.SetActive(locked);
	}

	public void unlockChar(){
		if(screws == numScrews && nuts == numNuts){
			locked = false;
			lockedPanel.gameObject.SetActive(locked);
			screws -= numScrews;
			nuts -= numNuts;

			GameObject.Find("Manager").GetComponent<EarnManager>().screws = screws;
			GameObject.Find("Manager").GetComponent<EarnManager>().nuts = nuts;
			GameObject.Find("Manager").GetComponent<EarnManager>().locked = false;
			GameObject.Find("Manager").GetComponent<SaveManager>().Save();
		}
	}
}
