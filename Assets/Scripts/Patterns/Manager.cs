﻿using UnityEngine;
using System.Collections;

public class Manager<T> : MonoBehaviour where T : MonoBehaviour {

    static T _instance = null;
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<T>();
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    public static bool HasInstance
    {
        get { return _instance != null;  }
    }

}
