﻿using UnityEngine;
using System.Collections;

public class UIController<T> : Singleton<T> where T : MonoBehaviour
{
    #region Unity Editor Members

    [Header("UI Controller")]
    [SerializeField]
    float _fadeInTime;
    [SerializeField]
    float _fadeOutTime;

    #endregion

    #region Private Members

    CanvasGroup _currentCanvas = null;

    #endregion

    #region Coroutines

    protected IEnumerator GoToCanvas(CanvasGroup canvas)
    {
        if (_currentCanvas != null)
        {
            yield return HideCanvas(_currentCanvas);
        }
        yield return ShowCanvas(canvas);

        _currentCanvas = canvas;

        yield return null;
    }

    protected IEnumerator HideCanvas(CanvasGroup canvas)
    {
        canvas.interactable = false;
        while (canvas.alpha > 0)
        {
            canvas.alpha -= Time.deltaTime / _fadeInTime;
            yield return new WaitForFixedUpdate();
        }
        canvas.alpha = 0;
        canvas.gameObject.SetActive(false);
        yield return null;
    }

    protected IEnumerator ShowCanvas(CanvasGroup canvas)
    {
        canvas.gameObject.SetActive(true);
        while (canvas.alpha < 1)
        {
            canvas.alpha += Time.deltaTime / _fadeOutTime;
            yield return new WaitForFixedUpdate();
        }
        canvas.alpha = 1;
        canvas.interactable = true;

        yield return null;
    }

    #endregion;
}
