﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ManagerSelection : Manager<ManagerSelection> {

    #region Private Members

    Character _character1;
    Character _character2;
    Character _character3;
    Character _character4;
    Character _characterSocial;

    #endregion

    #region Public Properties

    public Character Character1
    {
        get
        {
            return _character1;
        }

        set
        {
            _character1 = value;
        }
    }

    public Character Character2
    {
        get
        {
            return _character2;
        }

        set
        {
            _character2 = value;
        }
    }

    public Character Character3
    {
        get
        {
            return _character3;
        }

        set
        {
            _character3 = value;
        }
    }

    public Character Character4
    {
        get
        {
            return _character4;
        }

        set
        {
            _character4 = value;
        }
    }

    public Character CharacterSocial
    {
        get
        {
            return _characterSocial;
        }

        set
        {
            _characterSocial = value;
        }
    }

    #endregion

    /* public int i = 4;
	public int j = 4;
	public int k = 4;
	public int h = 4; */

    public void startRound(){
		ManagerSelection.DontDestroyOnLoad(this.gameObject);
        //SceneManager.LoadScene("Level" + GameObject.Find("Manager").GetComponent<EarnManager>().SceneLevel);
        //ScenesManager.Instance.LoadScene(ScenesManager.GAME_SCENE + EarnManager.Instance.SceneLevel);
		ScenesManager.Instance.LoadScene(ScenesManager.GAME_SCENE);
    }
}
