﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


[RequireComponent(typeof(Slider))]
public class LoadingProgressBar : MonoBehaviour
{
    Slider _slider;
    void Start() {
        _slider = GetComponent<Slider>();
    }

    void OnGUI() {
        _slider.value = SceneLoader.Progress;
    }
}
