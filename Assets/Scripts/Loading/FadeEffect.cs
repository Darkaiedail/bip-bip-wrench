﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CanvasGroup))]
public class FadeEffect : MonoBehaviour
{

    public bool autoFade = true;
    public float fadeSpeed = 0.75f;
    public int repeat = 0; // = infinite

    int count;

    CanvasGroup canvasGroup;

    // Use this for initialization
    void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        if (autoFade)
        {
            Play(fadeSpeed, repeat);
        }
    }

    public void Play(float fadeSpeed = 0.75f, int repeat = 0)
    {
        this.fadeSpeed = fadeSpeed;
        this.repeat = repeat;

        count = this.repeat > 0 ? this.repeat : 1;

        StartCoroutine("Playing");
    }

    public void Stop()
    {
        count = 0;
    }

    IEnumerator Playing()
    {
        while (count > 0)
        {
            while (canvasGroup.alpha > 0)
            {
                canvasGroup.alpha -= fadeSpeed * Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
            while (canvasGroup.alpha < 1)
            {
                canvasGroup.alpha += fadeSpeed * Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
            if (repeat > 0)
            {
                count--;
            }
        }
        canvasGroup.alpha = 1;
        yield return null;
    }
}
