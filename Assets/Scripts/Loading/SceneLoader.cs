﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneLoader : MonoBehaviour
{
    static AsyncOperation _loadOperation = null;
    public static float Progress {
        get { return _loadOperation == null ? 0 : _loadOperation.progress; }
    }


    /* void Start() {
        if (ScenesManager.Instance.CurrentScene != ScenesManager.Scene.LoadingScene) {
            StartCoroutine(Load());
        }
    }

    private IEnumerator Load() {
        _loadOperation = SceneManager.LoadSceneAsync((int)ScenesManager.Instance.CurrentScene);
        yield return null;
    }*/

    void Start()
    {
        if (ScenesManager.Instance.CurrentScene != ScenesManager.LOADING_SCENE)
        {
            StartCoroutine(Load());
        }
    }

    private IEnumerator Load()
    {
        _loadOperation = SceneManager.LoadSceneAsync(ScenesManager.Instance.CurrentScene);
        yield return null;
    }
}
