﻿using UnityEngine;
using System;

public class CharacterModel : ScriptableObject
{

    #region Constants & Enums

    public const string CHARACTER_PATH = "Characters/";
    public const string CHARACTER_NAME = "Character_{0}";
    public const string CHARACTER_FULLNAME = CHARACTER_PATH + CHARACTER_NAME;
    
    public enum CharacterElement
    {
        Ray,
        Fire,
        Plant,
        Water
    };

    #endregion

    #region Unity Editor Members

    //[Tooltip("Used to identify character and access to localized texts")]
    [SerializeField][HideInInspector]
    string _key;

    [Tooltip("Mark if the character can be used by player")]
    [SerializeField]
    bool _isPlayable;

    [Tooltip("Mark if players starts with this character")]
    [SerializeField]
    bool _isDefault;

    [SerializeField]
    CharacterElement _element;

    [SerializeField]
    Sprite _image;

    [SerializeField]
    Sprite _thumbnailImage;

    [SerializeField]
    Sprite _battlerImage;

    [SerializeField]
    int _price;

    #region Base Stats

    #region Life
    [Header("Life")]

    [Tooltip("Life at level 1")]
    [SerializeField]
    float _startLife;

    [Tooltip("Life at level 99")]
    [SerializeField]
    float _maxLife;

    [Tooltip("Life grow curve")]
    [SerializeField]
    AnimationCurve _lifeCurve;

    #endregion

    #region Damage
    [Header("Damage")]

    [Tooltip("Damage at level 1")]
    [SerializeField]
    float _startDamage;

    [Tooltip("Damage at level 99")]
    [SerializeField]
    float _maxDamage;

    [Tooltip("Damage grow curve")]
    [SerializeField]
    AnimationCurve _damageCurve;

    #endregion

    #region Reparation
    [Header("Reparation")]

    [Tooltip("Reparation at level 1")]
    [SerializeField]
    float _startReparation;

    [Tooltip("Reparation at level 99")]
    [SerializeField]
    float _maxReparation;

    [Tooltip("Reparation grow curve")]
    [SerializeField]
    AnimationCurve _reparationCurve;

    #endregion

    #region Taunt
    [Header("Taunt")]

    [Tooltip("Taunt at level 1")]
    [SerializeField]
    float _startTaunt;

    [Tooltip("Taunt at level 99")]
    [SerializeField]
    float _maxTaunt;

    [Tooltip("Taunt grow curve")]
    [SerializeField]
    AnimationCurve _tauntCurve;

    #endregion

    #endregion

    #region Hability

    //TODO: Change by Hability prefab?
    [Header("Hability")]

    [SerializeField]
    HabilityController.TypeOfHability _hability;

    [SerializeField]
    int _tokensToHability;

    [SerializeField]
    float _timeToRestartEnemyStats;

    [SerializeField]
    ParticleSystem _habilityReadyParticleSystem;

    #endregion

    #endregion

    #region Public Properties

    public string Key
    {
        get
        {
            return _key;
        }

        set
        {
            _key = value;
        }
    }

    public bool IsPlayable
    {
        get
        {
            return _isPlayable;
        }
    }

    public bool IsDefault
    {
        get
        {
            return _isDefault;
        }

        set
        {
            _isDefault = value;
        }
    }

    public CharacterElement Element
    {
        get
        {
            return _element;
        }

        set
        {
            _element = value;
        }
    }

    public Sprite Image
    {
        get
        {
            return _image;
        }
    }

    public Sprite ThumbnailImage
    {
        get
        {
            return _thumbnailImage;
        }
    }

    public Sprite BattlerImage
    {
        get
        {
            return _battlerImage;
        }

        set
        {
            _battlerImage = value;
        }
    }

    public float StartLife
    {
        get
        {
            return _startLife;
        }
    }

    public float MaxLife
    {
        get
        {
            return _maxLife;
        }
    }

    public AnimationCurve LifeCurve
    {
        get
        {
            return _lifeCurve;
        }
    }

    public float StartDamage
    {
        get
        {
            return _startDamage;
        }
    }

    public float MaxDamage
    {
        get
        {
            return _maxDamage;
        }
    }

    public AnimationCurve DamageCurve
    {
        get
        {
            return _damageCurve;
        }
    }

    public float StartReparation
    {
        get
        {
            return _startReparation;
        }
    }

    public float MaxReparation
    {
        get
        {
            return _maxReparation;
        }
    }

    public AnimationCurve DamageReparation
    {
        get
        {
            return _reparationCurve;
        }

        set
        {
            _reparationCurve = value;
        }
    }

    public float StartTaunt
    {
        get
        {
            return _startTaunt;
        }

        set
        {
            _startTaunt = value;
        }
    }

    public float MaxTaunt
    {
        get
        {
            return _maxTaunt;
        }

        set
        {
            _maxTaunt = value;
        }
    }

    public AnimationCurve DamageTaunt
    {
        get
        {
            return _tauntCurve;
        }

        set
        {
            _tauntCurve = value;
        }
    }

    public int Price
    {
        get
        {
            return _price;
        }

        set
        {
            _price = value;
        }
    }

    public HabilityController.TypeOfHability Hability
    {
        get
        {
            return _hability;
        }

        set
        {
            _hability = value;
        }
    }

    public int TokensToHability
    {
        get
        {
            return _tokensToHability;
        }

        set
        {
            _tokensToHability = value;
        }
    }

    public float TimeToRestartEnemyStats
    {
        get
        {
            return _timeToRestartEnemyStats;
        }

        set
        {
            _timeToRestartEnemyStats = value;
        }
    }

    public ParticleSystem HabilityReadyParticleSystem
    {
        get
        {
            return _habilityReadyParticleSystem;
        }

        set
        {
            _habilityReadyParticleSystem = value;
        }
    }

    #endregion

    public static CharacterModel GetCharacter(string key)
    {
        Debug.LogFormat("Getting character {0}", String.Format(CHARACTER_FULLNAME, key));

        return Resources.Load<CharacterModel>(String.Format(CHARACTER_FULLNAME, key));
    }

    public static CharacterModel[] LoadAll()
    {
        return Resources.LoadAll<CharacterModel>(CHARACTER_PATH);
    }

    public string GetName()
    {
        string nameLocalizationKey = String.Format("{0}_name", Key);
        return SmartLocalization.LanguageManager.Instance.HasKey(nameLocalizationKey) ?
                SmartLocalization.LanguageManager.Instance.GetTextValue(nameLocalizationKey) :
                Key;
    }

    public string GetDescription()
    {
        string descriptionLocalizationKey = String.Format("{0}_description", Key);
        return SmartLocalization.LanguageManager.Instance.HasKey(descriptionLocalizationKey) ?
                SmartLocalization.LanguageManager.Instance.GetTextValue(descriptionLocalizationKey) :
                Key;
    }

    public Character CreatePlayableCharacter(int level, int experience, bool owned = false)
    {
        //int _experienceToNextLevel;
        //int level = CharactersManager.Instance.GetLevelFromExperience(experience, out _experienceToNextLevel);

        return new Character(this, level, experience, owned);
    }

    public Character CreateEnemyCharacter(int level)
    {
        return new Character(this, level, 0);

    }

    public float GetBaseHealth(int level)
    {
        return _startLife + _lifeCurve.Evaluate((float)(level - CharactersManager.Instance.FirstLevel) / (float)(CharactersManager.Instance.LastLevel - CharactersManager.Instance.FirstLevel)) * (_maxLife - _startLife);
    }

    public float GetBaseDamage(int level)
    {
        return _startDamage + _damageCurve.Evaluate((float)(level - CharactersManager.Instance.FirstLevel) / (float)(CharactersManager.Instance.LastLevel - CharactersManager.Instance.FirstLevel)) * (_maxDamage - _startDamage);
    }

    public float GetBaseReparation(int level)
    {
        return _startReparation + _reparationCurve.Evaluate((float)(level - CharactersManager.Instance.FirstLevel) / (float)(CharactersManager.Instance.LastLevel - CharactersManager.Instance.FirstLevel)) * (_maxReparation - _startReparation);
    }

    public float GetBaseTaunt(int level)
    {
        return _startTaunt + _tauntCurve.Evaluate((float)(level - CharactersManager.Instance.FirstLevel) / (float)(CharactersManager.Instance.LastLevel - CharactersManager.Instance.FirstLevel)) * (_maxTaunt - _startTaunt);
    }
}
