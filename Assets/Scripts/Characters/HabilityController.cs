﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HabilityController : Singleton<HabilityController> {

	public enum TypeOfHability {None, Shield, Shake, Bomb, DoubleDamage, DoubleType};
	public TypeOfHability charHability;

	private int numYellowTokens;
	private int numRedTokens;
	private int numGreenTokens;
	private int numBlueTokens; 

	public int tokensToHability;

	public GameObject enemy;
	public float timeToRestartEnemyStats;
	public float startEnemyDamage;
	private bool firstRound = true;

	private GameObject[] gems;

	public int h;

	public ParticleSystem selectorPrefab;
	private ParticleSystem selector;
	private bool isInstantiateYellow = false;
	private bool isInstantiateRed = false;
	private bool isInstantiateGreen = false;
	private bool isInstantiateBlue = false;

	private float initialDamage;
	[HideInInspector]
	public int numTurns;

	void Start(){
		gems = GameObject.FindGameObjectsWithTag("Gem");

		StartCoroutine("startDamage");
    }

    public void enemySelected(GameObject enemySelec){
		enemy = enemySelec;
	}

	public void numberYellowTokensToHability(int numTokens){
		numYellowTokens += numTokens;
		isInstantiateYellow = false;
	}

	public void numberRedTokensToHability(int numTokens){
		numRedTokens += numTokens;
		isInstantiateRed = false;
	}

	public void numberGreenTokensToHability(int numTokens){
		numGreenTokens += numTokens;
		isInstantiateGreen = false;
	}

	public void numberBlueTokensToHability(int numTokens){
		numBlueTokens += numTokens;
		isInstantiateBlue = false;
	}

	void restartTokens(){
		numYellowTokens = 0;
		numRedTokens = 0;
		numGreenTokens = 0;
		numBlueTokens = 0; 
	}

	void Update () {
		instanceSelector();

		if(numTurns >= 4){
			numTurns = 0;
			this.gameObject.GetComponent<CharController>().charDamage /= 2;
			print("restart damage " + this.gameObject.GetComponent<CharController>().charDamage);

			GemManager.Instance.doubleHability = false;
		}
	}

	void instanceSelector(){
		if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Ray && numYellowTokens >= tokensToHability){
			if(!isInstantiateYellow){
				selector = Instantiate(selectorPrefab, this.transform.position, Quaternion.identity) as ParticleSystem;
				isInstantiateYellow = true;
			}
		}
		else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Fire && numRedTokens >= tokensToHability){
			if(!isInstantiateRed){
				selector = Instantiate(selectorPrefab, this.transform.position, Quaternion.identity) as ParticleSystem;
				isInstantiateRed = true;
			}
		}
		else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Plant && numGreenTokens >= tokensToHability){
			if(!isInstantiateGreen){
				selector = Instantiate(selectorPrefab, this.transform.position, Quaternion.identity) as ParticleSystem;
				isInstantiateGreen = true;
			}
		}
		else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Water && numBlueTokens >= tokensToHability){
			if(!isInstantiateBlue){
				selector = Instantiate(selectorPrefab, this.transform.position, Quaternion.identity) as ParticleSystem;
				isInstantiateBlue = true;
			}
		}
	}

	void doHability(){
		if(enemy != null){
			switch(charHability){
			case TypeOfHability.Shield:
				print("shield");
				restartTokens();

				enemy.GetComponent<EnemyController>().enemyDamage -= enemy.GetComponent<EnemyController>().enemyDamage;
				enemy.GetComponent<EnemyController>().shield = true;

				DestroySelector();

				break;

			case TypeOfHability.Shake:
				print("shake");
				restartTokens();

				if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Ray){
					int i = 0;
					while(i < 5){
						i++;
						int randomTile = Random.Range(0,gems.Length);
						gems[randomTile].GetComponent<GemManager>().destroyYellow();
					}
				}
				else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Fire){
					int i = 0;
					while(i < 5){
						i++;
						int randomTile = Random.Range(0,gems.Length);
						gems[randomTile].GetComponent<GemManager>().destroyRed();
					}
				}
				else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Plant){
					int i = 0;
					while(i < 5){
						i++;
						int randomTile = Random.Range(0,gems.Length);
						gems[randomTile].GetComponent<GemManager>().destroyGreen();
					}
				}
				else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Water){
					int i = 0;
					while(i < 5){
						i++;
						int randomTile = Random.Range(0,gems.Length);
						gems[randomTile].GetComponent<GemManager>().destroyBlue();
					}
				}

				DestroySelector();

				break;

			case TypeOfHability.Bomb:
				print("bomb");
				restartTokens();

				break;

			case TypeOfHability.DoubleDamage:
				print("damage x2");
				restartTokens();

				GemManager.Instance.doubleHability = true;

//				initialDamage = this.gameObject.GetComponent<CharController>().charDamage;
//				print("initial damage " + initialDamage);

				this.gameObject.GetComponent<CharController>().charDamage *= 2;
				print("double damage " + this.gameObject.GetComponent<CharController>().charDamage);

				DestroySelector();

				break;

			case TypeOfHability.DoubleType:
				print("double type");
				restartTokens();

				break;
			}
		}
	}

	void DestroySelector() {
		if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Ray){
			Destroy(selector);
		}
		else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Fire){
			Destroy(selector);
		}
		else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Plant){
			Destroy(selector);
		}
		else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Water){
			Destroy(selector);
		}
	}

	void OnMouseOver() {
		if(Input.GetMouseButtonDown(0)){
			if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Ray && numYellowTokens >= tokensToHability){
				doHability();
			}
			else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Fire && numRedTokens >= tokensToHability){
				doHability();
			}
			else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Plant && numGreenTokens >= tokensToHability){
				doHability();
			}
			else if(this.gameObject.GetComponent<CharController>().charType == CharacterModel.CharacterElement.Water && numBlueTokens >= tokensToHability){
				doHability();
			}
		}
	}

	IEnumerator startDamage(){
		yield return new WaitForSeconds(.5f);
		if(enemy != null){
			startEnemyDamage = enemy.GetComponent<EnemyController>().enemyDamage;
			print("start enemy damage");
		}
	}

	public void restartEnemyDamage(){
		enemy.GetComponent<EnemyController>().enemyDamage = startEnemyDamage;
		print("initial damage");
	}
}
