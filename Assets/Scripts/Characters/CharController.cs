﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharController : MonoBehaviour {

	//public enum TypeOfChar {Ray, Fire, Plant, Water}; //MLR: Get from Character model
	public CharacterModel.CharacterElement charType;

	public enum ClassOfChar{Tank, Special, Damage, Balance};
	public ClassOfChar charClass;

    public Character character;

    public float charDamage;
	public float life;
	public float health;
	public float heartsValue;
    public float taunt;

    public GameObject enemy;
	public GameObject[] enemyArray;

	private int numYellowTokens;
	private int numRedTokens;
	private int numGreenTokens;
	private int numBlueTokens; 

	public GameObject[] healthBar;
	public Text damagePrefab;
	private Text damageTextPrefab;
	public GameObject canvas;

	public GameObject[] damageText;
	public GameObject[] cureText;
	public int h;

	[HideInInspector]
	public bool firstRound;

	[HideInInspector]
	public bool mazEnd;

	public bool changeStats;

	[HideInInspector]
	public bool combo;

	private ComboController comboController;

	public void numberYellowTokensDestroy(int numTokens){
		numYellowTokens += numTokens;
		print("numYellowTokens " + numYellowTokens);
	}

	public void numberRedTokensDestroy(int numTokens){
		numRedTokens += numTokens;
		print("numRedTokens " + numRedTokens);
	}

	public void numberGreenTokensDestroy(int numTokens){
		numGreenTokens += numTokens;
		print("numGreenTokens " + numGreenTokens);
	}

	public void numberBlueTokensDestroy(int numTokens){
		numBlueTokens += numTokens;
		print("numBlueTokens " + numBlueTokens);
	}

	public void restartNumTokens(){
		numYellowTokens = 0;
		numRedTokens = 0;
		numGreenTokens = 0;
		numBlueTokens = 0;
	}

	public void enemySelected(GameObject enemySelec){
		enemy = enemySelec;
		comboController.enemy = enemySelec;
	}

	/*void Awake(){
		canvas = GameObject.Find("Canvas");
		healthBar = GameObject.FindGameObjectsWithTag("HealthBar");
		damageText = GameObject.FindGameObjectsWithTag("DamageText");
	}*/

	void Start(){
		comboController = GameObject.Find("GameManager").GetComponent<ComboController>();

        canvas = GameObject.Find("Canvas");
        healthBar = GameObject.FindGameObjectsWithTag("HealthBar");
        damageText = GameObject.FindGameObjectsWithTag("DamageText");
		cureText = GameObject.FindGameObjectsWithTag("CureText");

        /*life = gameObject.GetComponent<LevelController>().life;
		health = life;
		charDamage = gameObject.GetComponent<LevelController>().damage;*/
    }

	void selectFirstRandomEnemy(){
		enemyArray = GameObject.FindGameObjectsWithTag("Enemy");

		int i = GameObject.Find("Board").GetComponent<BoardManager>().randomEnemy;
		enemySelected(enemyArray[i]);
		gameObject.GetComponent<HabilityController>().enemySelected(enemyArray[i]);
		enemy.gameObject.GetComponent<EnemyController>().selector.SetActive(false);
		enemy.gameObject.GetComponent<EnemyController>().selector.SetActive(true);
	}

	public void selectRandomEnemy(){
		enemyArray = null;

		StartCoroutine("findEnemies");
	}

	IEnumerator findEnemies(){
		yield return new WaitForSeconds(0.5f);

		if(!mazEnd && enemyArray == null){
			enemyArray = GameObject.FindGameObjectsWithTag("Enemy");
			int i = 0;
			enemySelected(enemyArray[i]);
			gameObject.GetComponent<HabilityController>().enemySelected(enemyArray[i]);
			enemy.gameObject.GetComponent<EnemyController>().selector.SetActive(false);
			enemy.gameObject.GetComponent<EnemyController>().selector.SetActive(true);
		}
	}

	void Update(){

		if(!mazEnd){
			if(firstRound){
				selectFirstRandomEnemy();
				firstRound = false;
			}

			if(charType == CharacterModel.CharacterElement.Ray){
				if(enemy != null){
					if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Ray && numYellowTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numYellowTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Ray)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Ray] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Ray, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Fire && numYellowTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numYellowTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Ray)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Ray] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Ray, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Plant && numYellowTokens != 0){
						float baseDamage = 0.5f;

						float totalDamage = numYellowTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Ray)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Ray] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Ray, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Water && numYellowTokens != 0){
						float baseDamage = 2.0f;

						float totalDamage = numYellowTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Ray)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Ray] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Ray, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
				}
			}
			else if(charType == CharacterModel.CharacterElement.Fire){
				if(enemy != null){
					if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Ray && numRedTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numRedTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Fire)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Fire] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Fire, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Fire && numRedTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numRedTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Fire)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Fire] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Fire, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Plant && numRedTokens != 0){
						float baseDamage = 2.0f;

						float totalDamage = numRedTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Fire)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Fire] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Fire, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Water && numRedTokens != 0){
						float baseDamage = 0.5f;

						float totalDamage = numRedTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Fire)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Fire] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Fire, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
				}
			}
			else if(charType == CharacterModel.CharacterElement.Plant){
				if(enemy != null){
					if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Ray && numGreenTokens != 0){
						float baseDamage = 2.0f;

						float totalDamage = numGreenTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Plant)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Plant] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Plant, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Fire && numGreenTokens != 0){
						float baseDamage = 0.5f;

						float totalDamage = numGreenTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Plant)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Plant] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Plant, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Plant && numGreenTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numGreenTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Plant)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Plant] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Plant, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Water && numGreenTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numGreenTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Plant)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Plant] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Plant, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
				}
			}
			else if(charType == CharacterModel.CharacterElement.Water){
				if(enemy != null){
					if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Ray && numBlueTokens != 0){
						float baseDamage = 0.5f;

						float totalDamage = numBlueTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Water)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Water] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Water, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Fire && numBlueTokens != 0){
						float baseDamage = 2.0f;

						float totalDamage = numBlueTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Water)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Water] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Water, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Plant && numBlueTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numBlueTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Water)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Water] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Water, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
					else if(enemy.GetComponent<EnemyController>().type == CharacterModel.CharacterElement.Water && numBlueTokens != 0){
						float baseDamage = 1.0f;

						float totalDamage = numBlueTokens * charDamage * baseDamage * 0.4f;
						enemy.GetComponent<EnemyController>().kill(totalDamage);

						if(!combo){
							printDamage(totalDamage);
						}
						else {
							if(comboController.damageDictionary.ContainsKey(CharacterModel.CharacterElement.Water)){
								comboController.damageDictionary[CharacterModel.CharacterElement.Water] += totalDamage;
							}
							else{
								comboController.damageDictionary.Add(CharacterModel.CharacterElement.Water, totalDamage);
							}

							comboController.totalDamage();
						}

						restartNumTokens();
					}
				}
			}

			if(health > life){
				health = life;
			}

			if(health <= 0){
				die();
			}
		}
	}

	public void defeat(float damage){
		health -= damage;

		float healthSize = health/life;
		healthBar[h].GetComponent<Scrollbar>().size = healthSize;
	}

	public void cure(int hearts){
		float totalCure = hearts * heartsValue;
		health += totalCure;

		float healthSize = health/life;
		healthBar[h].GetComponent<Scrollbar>().size = healthSize;

		printCharCure(totalCure);
	}

	void die(){
		damageText[h].GetComponent<Text>().gameObject.SetActive(false);
		cureText[h].GetComponent<Text>().gameObject.SetActive(false);

		damageTextPrefab.gameObject.SetActive(false);

		healthBar[h].GetComponent<Scrollbar>().gameObject.SetActive(false);

		StartCoroutine("FindCharactersAndEnemies");

		Destroy(this.gameObject);
	}

	IEnumerator FindCharactersAndEnemies(){
		yield return new WaitForSeconds(.5f);

		enemyArray = GameObject.FindGameObjectsWithTag("Enemy");

		GemManager.Instance.FindCharacters();

		for(int i = 0; i < enemyArray.Length; i++){
			enemyArray[i].GetComponent<EnemyController>().findCharacters();
		}

		GameObject.Find("GameManager").GetComponent<ComboController>().FindCharacters();
	}

	public void printDamage(float damage){

		if(enemy != null){
			if(damageTextPrefab == null){
				damageTextPrefab = Instantiate(damagePrefab,enemy.transform.position, Quaternion.identity) as Text;
				damageTextPrefab.transform.SetParent(canvas.transform, false);
			}
		}

		damageTextPrefab.transform.position = enemy.transform.position;

		damageTextPrefab.gameObject.SetActive(true);
		damageTextPrefab.color = Color.black;

		iTween.MoveAdd(damageTextPrefab.gameObject, iTween.Hash("x", .5f,
			"time", .5f, "easetype","easeOutBounce"));
		iTween.ScaleAdd(damageTextPrefab.gameObject, iTween.Hash("y", .5f, "x", .5f,
			"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

		string turnsStr = damage.ToString("00");
		damageTextPrefab.text = turnsStr;

		StartCoroutine("desactiveDamage");
	}

	IEnumerator desactiveDamage(){
		yield return new WaitForSeconds(1.0f);

		damageTextPrefab.gameObject.SetActive(false);
		Destroy(damageTextPrefab);
		Destroy(GameObject.Find("EnemyDamage(Clone)"));
	}

	public void printCharDamage(float damage){
		damageText[h].GetComponent<Text>().gameObject.SetActive(true);
		damageText[h].GetComponent<Text>().color = Color.red;

		iTween.MoveAdd(damageText[h].gameObject, iTween.Hash("y", .5f,
			"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));
		iTween.ScaleAdd(damageText[h].gameObject, iTween.Hash("y", .005f, "x", .005f,
			"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

		string turnsStr = damage.ToString("00");
		damageText[h].GetComponent<Text>().text = "-" + turnsStr;

		StartCoroutine("desactiveCharDamage");
	}

	void printCharCure(float heart){
		cureText[h].GetComponent<Text>().gameObject.SetActive(true);
		cureText[h].GetComponent<Text>().color = Color.magenta;

		iTween.ScaleAdd(cureText[h].gameObject, iTween.Hash("y", .005f, "x", .005f,
			"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

		string turnsStr = heart.ToString("00");
		cureText[h].GetComponent<Text>().text = "+" + turnsStr;

		StartCoroutine("desactiveCharDamage");
	}

	IEnumerator desactiveCharDamage(){
		yield return new WaitForSeconds(1.0f);

		damageText[h].GetComponent<Text>().gameObject.SetActive(false);
		cureText[h].GetComponent<Text>().gameObject.SetActive(false);
	}
}
