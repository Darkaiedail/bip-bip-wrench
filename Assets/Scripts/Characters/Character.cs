﻿using UnityEngine;
using System.Collections;


public class Character {

    [System.Serializable]
    public class SaveData
    {
        #region Private Members

        [SerializeField]
        string _key;

        [SerializeField]
        int _level;

        [SerializeField]
        int _experience;

        [SerializeField]
        bool _owned;

        [SerializeField]
        float _resurrectTime;

        #endregion

        #region Public Properties

        public string Key
        {
            get
            {
                return _key;
            }

            set
            {
                _key = value;
            }
        }

        public int Level
        {
            get
            {
                return _level;
            }

            set
            {
                _level = value;
            }
        }

        public int Experience
        {
            get
            {
                return _experience;
            }

            set
            {
                _experience = value;
            }
        }

        public bool Owned
        {
            get
            {
                return _owned;
            }

            set
            {
                _owned = value;
            }
        }

        public float ResurrectTime
        {
            get
            {
                return _resurrectTime;
            }

            set
            {
                _resurrectTime = value;
            }
        }

        #endregion

        public SaveData(string key, int level, int experience, bool owned = false, float dieTime = 0)
        {
            _key = key;
            Level = level;
            _experience = experience;
            _owned = owned;
            ResurrectTime = 0;
        }
    }

    #region Private Members

    CharacterModel _model;

    int _level;

    int _totalExperience;

    int _experienceToNextLevel;

    float _baseHealth;

    float _baseDamage;

    float _baseReparation;

    float _baseTaunt;

    bool _owned;

    float _resurrectTime = 0;

    bool _canLevelUp = false;

    int _levelUpPrice;
    
    #endregion

    #region Public Properties

    public CharacterModel Model
    {
        get
        {
            return _model;
        }
    }


    public int Level
    {
        get
        {
            return _level;
        }
        set
        {
            _level = value;

            _baseHealth = _model.GetBaseHealth(_level);
            _baseDamage = _model.GetBaseDamage(_level);
            _baseReparation = _model.GetBaseReparation(_level);
            _baseTaunt = _model.GetBaseTaunt(_level);
            _levelUpPrice = CharactersManager.Instance.GetLevelUpPrice(_level + 1);
        }
    }

    public int TotalExperience
    {
        get
        {
            return _totalExperience;
        }

        set
        {
            _totalExperience = value;

            int level = CharactersManager.Instance.GetLevelFromExperience(_totalExperience, out _experienceToNextLevel);
            if (level != _level)
            {
                _canLevelUp = true;
            }
        }
    }

    public int ExperienceToNextLevel
    {
        get
        {
            return _experienceToNextLevel;
        }
    }

    public float BaseHealth
    {
        get
        {
            return _baseHealth;
        }
    }

    public float BaseDamage
    {
        get
        {
            return _baseDamage;
        }
    }

    public float BaseReparation
    {
        get
        {
            return _baseReparation;
        }
    }

    public float BaseTaunt
    {
        get
        {
            return _baseTaunt;
        }
    }

    public bool Owned
    {
        get
        {
            return _owned;
        }

        set
        {
            _owned = value;
        }
    }

    public float ResurrectTime
    {
        get
        {
            return _resurrectTime;
        }

        set
        {
            _resurrectTime = value;
        }
    }

    public bool CanLevelUp
    {
        get
        {
            return _canLevelUp;
        }
    }

    public int LevelUpPrice
    {
        get
        {
            return _levelUpPrice;
        }
    }

    #endregion

    public Character(CharacterModel model, int level, int experience, bool owned = false) {

        _model = model;
        _owned = owned;

        Level = level;
        TotalExperience = experience;
    }
}
