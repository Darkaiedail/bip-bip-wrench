﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ComboController : MonoBehaviour {

	private GameObject[] characters;

	public Text damagePrefab;
	private Text damageTextPrefab;
	public GameObject canvas;
	public GameObject enemy;

	public Dictionary<CharacterModel.CharacterElement, float> damageDictionary = new Dictionary<CharacterModel.CharacterElement, float>();

	void Start(){
		FindCharacters();
	}

	public void FindCharacters(){
		characters = GameObject.FindGameObjectsWithTag("Player");
	}

	public void totalDamage(){
		float[] damages = new float[damageDictionary.Count];
		damageDictionary.Values.CopyTo(damages,0);

		StartCoroutine(printAllDamage(damages));
	}

	IEnumerator printAllDamage(float[] damages){
		foreach(float damage in damages){
			printDamage(damage);
			yield return new WaitForSeconds(.5f);
		}

		damageDictionary.Clear();

		FindCharacters();

		for(int i = 0; i < characters.Length; i++){
			characters[i].GetComponent<CharController>().combo = false;
		}

		yield return null;
	}

	void printDamage(float damage){
		if(enemy != null){
			if(damageTextPrefab == null){
				damageTextPrefab = Instantiate(damagePrefab,enemy.transform.position, Quaternion.identity) as Text;
				damageTextPrefab.transform.SetParent(canvas.transform, false);
			}

			damageTextPrefab.transform.position = enemy.transform.position;

			damageTextPrefab.gameObject.SetActive(true);
			damageTextPrefab.color = Color.black;

			iTween.MoveAdd(damageTextPrefab.gameObject, iTween.Hash("x", .5f,
				"time", .5f, "easetype","easeOutBounce"));
			iTween.ScaleAdd(damageTextPrefab.gameObject, iTween.Hash("y", .5f, "x", .5f,
				"time", .5f, "easetype","easeOutQuint", "looptype", "pingPong"));

			string turnsStr = damage.ToString("00");
			damageTextPrefab.text = turnsStr;

			StartCoroutine("desactiveDamage");
		}
	}

	IEnumerator desactiveDamage(){
		yield return new WaitForSeconds(.5f);

		if(damageTextPrefab != null){
			damageTextPrefab.gameObject.SetActive(false);
		}
	}
}
