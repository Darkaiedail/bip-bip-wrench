﻿using UnityEngine;
using System.Collections;

class ScenesManager : Manager<ScenesManager>
{
    public const string LOADING_SCENE = "Loading";
    public const string MENU_SCENE = "Menu";
    public const string GAME_SCENE = "Game";

    /*public enum Scene
    {
        LoadingScene,
        MenuScene,
        GameScene
    }

    Scene _currentScene = Scene.MenuScene;
    public Scene CurrentScene
    {
        get { return _currentScene; }
    }

    public void LoadScene(Scene scene)
    {
        if (scene == Scene.LoadingScene){
            Debug.LogWarning("[SceneManager] Some script is attempting to load the Loading Scene");
            return;
        }

        //AnalyticsManager.LoadSceneEvent(_currentScene.ToString());

        _currentScene = scene;
        UnityEngine.SceneManagement.SceneManager.LoadScene((int)Scene.LoadingScene);
    }*/

    string _currentScene;
    public string CurrentScene
    {
        get { return _currentScene; }
    }

    public void LoadScene(string scene)
    {
        if (scene == LOADING_SCENE)
        {
            Debug.LogWarning("[SceneManager] Some script is attempting to load the Loading Scene");
            return;
        }

        //AnalyticsManager.LoadSceneEvent(_currentScene.ToString());

        _currentScene = scene;
        UnityEngine.SceneManagement.SceneManager.LoadScene(LOADING_SCENE);
    }
}