﻿using UnityEngine;
using SmartLocalization;


public class SettingsManager : Manager<SettingsManager>
{
    string _language = "en";
    public string Language
    {
        get { return _language; }
    }

    void Start()
    {
        //Read settings

        //else default
        SetDefaultLanguage();

        LanguageManager.Instance.ChangeLanguage(_language);

    }

    void SetDefaultLanguage()
    {
        switch (Application.systemLanguage)
        {
            case SystemLanguage.Spanish:
                _language = "es";
                break;

            //TODO: add languages

            default:
                _language = "en";
                break;
        }
    }
}
