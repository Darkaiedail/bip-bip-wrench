﻿using UnityEngine;
using System.Collections.Generic;

public class CharactersManager : Manager<CharactersManager> {

    #region Unity Editor Members 

    [SerializeField]
    int _firstLevel = 1;

    [SerializeField]
    int _lastLevel = 99;

    [SerializeField]
    int _firstLevelExperience = 100;

    [SerializeField]
    int _lastLevelExperience = 1000000;

    [SerializeField]
    AnimationCurve _experienceCurve;

    [SerializeField]
    [Tooltip("Price for level up from first level to next level")]
    int _firstLevelUpPrice = 10;

    [SerializeField]
    int _lastLevelUpPrice = 1000000; //1M

    [SerializeField]
    AnimationCurve _levelUpPriceCurve;

    [SerializeField]
    int _maxHealth = 9999;

    [SerializeField]
    int _maxDamage = 9999;

    [SerializeField]
    int _maxReparation = 999;

    [SerializeField]
    int _maxTaunt = 99;

    #endregion

    #region Private Members

    bool _initialized = false;

    Dictionary<string, Character> _playableCharacters = new Dictionary<string, Character>();
    //Dictionary<string, Character> _enemyCharacters = new Dictionary<string, Character>();

    #endregion

    #region Public properties

    public Dictionary<string, Character> PlayableCharacters
    {
        get { return _playableCharacters; }
    }

    public int FirstLevel
    {
        get
        {
            return _firstLevel;
        }
    }

    public int FirstLevelExperience
    {
        get
        {
            return _firstLevelExperience;
        }

        set
        {
            _firstLevelExperience = value;
        }
    }

    public int LastLevel
    {
        get
        {
            return _lastLevel;
        }
    }

    public int LastLevelExperience
    {
        get
        {
            return _lastLevelExperience;
        }
    }

    public int MaxHealth
    {
        get
        {
            return _maxHealth;
        }
    }

    public int MaxDamage
    {
        get
        {
            return _maxDamage;
        }
    }

    public int MaxReparation
    {
        get
        {
            return _maxReparation;
        }
    }

    public int MaxTaunt
    {
        get
        {
            return _maxTaunt;
        }
    }


    /*public Dictionary<string, Character> EnemyCharacters
    {
        get { return _enemyCharacters; }
    }*/

    #endregion

    // Use this for initialization
    /*void Awake () {
        CharacterModel[] characters = CharacterModel.LoadAll();
        foreach (CharacterModel character in characters)
        {
            if (character.IsPlayable)
            {
                Character.SaveData characterData;
                if (EarnManager.Instance.characters.ContainsKey(character.Key)) {
                    characterData = EarnManager.Instance.characters[character.Key];
                } else
                {
                    characterData = new Character.SaveData(character.Key, _firstLevel, FirstLevelExperience);
                }
                _playableCharacters.Add(characterData.Key, character.CreatePlayableCharacter(characterData.Level, characterData.Experience, characterData.Owned));
            }
            /*else
            {
                _enemyCharacters.Add(character.Key, character);
            }
        }
	}*/

    void Initialize()
    {
        if (!_initialized)
        {
            CharacterModel[] characters = CharacterModel.LoadAll();
            foreach (CharacterModel character in characters)
            {
                if (character.IsPlayable)
                {
                    Character.SaveData characterData;
                    if (EarnManager.Instance.characters.ContainsKey(character.Key))
                    {
                        characterData = EarnManager.Instance.characters[character.Key];
                    }
                    else
                    {
                        characterData = new Character.SaveData(character.Key, _firstLevel, FirstLevelExperience);
                    }
                    _playableCharacters.Add(characterData.Key, character.CreatePlayableCharacter(characterData.Level, characterData.Experience, characterData.Owned));
                }
            }

            _initialized = true;
        }
    }

    public void RefreshCharacters()
    {
        if (!_initialized)
        {
            Initialize();
        }
        else
        {
            foreach (KeyValuePair<string, Character> character in _playableCharacters)
            {
                Character.SaveData characterData;
                if (EarnManager.Instance.characters.ContainsKey(character.Key))
                {
                    characterData = EarnManager.Instance.characters[character.Key];
                }
                else
                {
                    characterData = new Character.SaveData(character.Key, _firstLevel, FirstLevelExperience);
                }

                //_playableCharacters[character.Key] = character.Value.Model.CreatePlayableCharacter(characterData.Level, characterData.Experience, characterData.Owned);

                character.Value.Level = characterData.Level;
                character.Value.TotalExperience = characterData.Experience;
                character.Value.Owned = characterData.Owned;
            }
        }
    }

    public int GetLevelUpPrice(int nextLevel)
    {
        return _firstLevelUpPrice + (int)(_levelUpPriceCurve.Evaluate((float)(nextLevel - FirstLevel) / (float)(LastLevel - FirstLevel)) * (_lastLevelUpPrice - _firstLevelUpPrice));
    }

    public int GetLevelExperience(int level)
    {
        return FirstLevelExperience + (int)(_experienceCurve.Evaluate((float)(level - FirstLevel) / (float)(LastLevel - FirstLevel)) * (LastLevelExperience - FirstLevelExperience));
    }

    public int GetLevelFromExperience(int totalExperience, out int experienceToNextLevel)
    {
        int level;
        for (level = FirstLevel; level < LastLevel; ++level)
        {
            int experienceNeeded = GetLevelExperience(level);
            if (totalExperience < experienceNeeded)
            {
                experienceToNextLevel = experienceNeeded - totalExperience;
                return level - 1;
            }
        }

        Debug.LogErrorFormat("Experience out of bounds");

        experienceToNextLevel = 0;
        return 1;
    }
}
