﻿using UnityEngine;
using UnityEngine.UI;

public class WorldLevelController : MonoBehaviour {

    [SerializeField]
    int _worldIndex;

    [SerializeField]
    Image[] _levelLockedImages;

	// Use this for initialization
	void Start () {
        for (int i = 1; i < LockLevel.levels; i++)
        {
            int levelIndex = i + 1;
            if ((PlayerPrefs.GetInt("level_" + _worldIndex.ToString() + "-" + levelIndex.ToString())) == 1)
            {
                _levelLockedImages[i].gameObject.SetActive(false);
                _levelLockedImages[i].transform.parent.gameObject.GetComponent<Button>().interactable = true;
                Debug.Log("Unlocked");
            }
        }
    }
}
