﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class LevelBriefingController : MonoBehaviour {

#region Unity Editor Members

    [SerializeField]
    Text _levelNameText;

    [SerializeField]
    Image _finalCard1Box;

    [SerializeField]
    Image _finalCard2Box;

    [SerializeField]
    Image _finalCard3Box;

    [SerializeField]
    Image _finalCard4Box;

    [SerializeField]
    Image _finalCardSocialBox;

#endregion

    #region Public properties

    public Image FinalCard1Box
    {
        get
        {
            return _finalCard1Box;
        }

        set
        {
            _finalCard1Box = value;
        }
    }

    public Image FinalCard2Box
    {
        get
        {
            return _finalCard2Box;
        }

        set
        {
            _finalCard2Box = value;
        }
    }

    public Image FinalCard3Box
    {
        get
        {
            return _finalCard3Box;
        }

        set
        {
            _finalCard3Box = value;
        }
    }

    public Image FinalCard4Box
    {
        get
        {
            return _finalCard4Box;
        }

        set
        {
            _finalCard4Box = value;
        }
    }

    public Image FinalCardSocialBox
    {
        get
        {
            return _finalCardSocialBox;
        }

        set
        {
            _finalCardSocialBox = value;
        }
    }

    #endregion

    void OnEnable()
    {
        _levelNameText.text = String.Format(SmartLocalization.LanguageManager.Instance.GetTextValue("LevelTitle"), EarnManager.Instance.SceneLevel);
    }
}
