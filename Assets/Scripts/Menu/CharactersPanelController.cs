﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public abstract class CharactersPanelController : MonoBehaviour {

    #region  Unity Editor members

    [SerializeField]
    protected CharacterCardButton _characterCardButtonPrefab;

    [SerializeField]
    protected GridLayoutGroup _adquiredCharacters;

    [SerializeField]
    protected GridLayoutGroup _promotionCharacters;

    #endregion 

    //TODO: Change by Sprites
    [SerializeField]
    Color _rayBackgroundColor;

    [SerializeField]
    Color _fireBackgroundColor;

    [SerializeField]
    Color _plantBackgroundColor;

    [SerializeField]
    Color _waterBackgroundColor;

    //TODO: Change By GetElementBackgroundSprite
    public Color GetElementColor(CharacterModel.CharacterElement element)
    {
        switch (element)
        {
            case CharacterModel.CharacterElement.Ray:
                return _rayBackgroundColor;

            case CharacterModel.CharacterElement.Fire:
                return _fireBackgroundColor;

            case CharacterModel.CharacterElement.Plant:
                return _plantBackgroundColor;

            case CharacterModel.CharacterElement.Water:
                return _waterBackgroundColor;
        }

        return Color.white;
    }
}
