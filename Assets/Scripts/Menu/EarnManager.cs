﻿using UnityEngine;
using System.Collections.Generic;

public class EarnManager : Manager<EarnManager>
{

    int _coins;
    public int Coins
    {
        get { return _coins; }
        set
        {
            _coins = value;
            if (OnSoftMoneyChange != null) {
                OnSoftMoneyChange(_coins);
            }
        }
    }
    public Dictionary<string, Character.SaveData> characters = new Dictionary<string, Character.SaveData>();

    /*public int expRayMimi;
	public int expFireMimi;
	public int expPlantMimi;
	public int expWaterMimi;
	public int expSpecialMimi;

	public int[] level = new int[5];
	public int[] life = new int[5];
	public int[] damage = new int[5];

	public int canLevelUpRayMimi;
	public int canLevelUpFireMimi;
	public int canLevelUpPlantMimi;
	public int canLevelUpWaterMimi;
	public int canLevelUpSpecialMimi;

	private bool canPayCoinRayMimi;
	private bool canPayCoinFireMimi;
	private bool canPayCoinPlantMimi;
	private bool canPayCoinWaterMimi;
	private bool canPayCoinSpecialMimi;

	public int nextLevelExpRayMimi = 400;
	public int nextLevelExpFireMimi = 400;
	public int nextLevelExpPlantMimi = 400;
	public int nextLevelExpWaterMimi = 400;
	public int nextLevelExpSpecialMimi = 400;

	public int nextLevelCoinRayMimi = 100;
	public int nextLevelCoinFireMimi = 100;
	public int nextLevelCoinPlantMimi = 100;
	public int nextLevelCoinWaterMimi = 100;
	public int nextLevelCoinSpecialMimi = 100;*/

    [HideInInspector]
    public int nextLevel;
    public int multiplier;

    public int screws;
    public int nuts;
    public bool locked = true;

    string _sceneLevel;
    public string SceneLevel
    {
        get
        {
            return _sceneLevel;
        }
        set
        {
            _sceneLevel = value;

            if (_sceneLevel.Equals("1-1"))
            {
                multiplier = 1;
            }
            if (_sceneLevel.Equals("1-2"))
            {
                multiplier = 1;
            }
            if (_sceneLevel.Equals("1-3"))
            {
                nextLevel = 2;
            }
            if (_sceneLevel.Equals("1-4"))
            {
                multiplier = 2;
            }
            if (_sceneLevel.Equals("1-5"))
            {
                multiplier = 3;
            }
            if (_sceneLevel.Equals("1-6"))
            {
                multiplier = 3;
            }
            if (_sceneLevel.Equals("1-7"))
            {
                multiplier = 3;
            }
            if (_sceneLevel.Equals("1-8"))
            {
                multiplier = 4;
            }
            if (_sceneLevel.Equals("1-9"))
            {
                multiplier = 4;
            }
            if (_sceneLevel.Equals("1-10"))
            {
                multiplier = 7;
            }
        }
    }


    void Awake()
    {
        CharactersManager.Instance.RefreshCharacters();
        SaveManager.Instance.Load();
    }

    public void UpdateCharacters()
    {
        foreach (Character character in CharactersManager.Instance.PlayableCharacters.Values)
        {
            if (characters.ContainsKey(character.Model.Key))
            {
                characters[character.Model.Key].Level = character.Level;
                characters[character.Model.Key].Experience = character.TotalExperience;
                characters[character.Model.Key].Owned = character.Owned;
                characters[character.Model.Key].ResurrectTime = character.ResurrectTime;
            }
            else
            {
                EarnManager.Instance.characters.Add(character.Model.Key, new Character.SaveData(
                                character.Model.Key,
                                CharactersManager.Instance.FirstLevel,
                                CharactersManager.Instance.FirstLevelExperience,
                                character.Owned,
                                character.ResurrectTime));
            }
        }
    }

    #region Events

    public delegate void OnSoftMoneyChangeEventHandler(int value);
    public event OnSoftMoneyChangeEventHandler OnSoftMoneyChange;

    #endregion
}