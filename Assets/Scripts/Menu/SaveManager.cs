﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

public class SaveManager : Singleton<SaveManager> {

    public void Save(){
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

		PlayerData data = new PlayerData();
		//data.nextLevel = gameObject.GetComponent<EarnManager>().nextLevel;
		data.coins = EarnManager.Instance.Coins;
		data.screws = EarnManager.Instance.screws;
		data.nuts = EarnManager.Instance.nuts;

        data.characters = new Character.SaveData[EarnManager.Instance.characters.Count];
        EarnManager.Instance.characters.Values.CopyTo(data.characters, 0);

        data.selectedCharacter1 = ManagerSelection.Instance.Character1 != null ? ManagerSelection.Instance.Character1.Model.Key : null;
        data.selectedCharacter2 = ManagerSelection.Instance.Character2 != null ? ManagerSelection.Instance.Character2.Model.Key : null;
        data.selectedCharacter3 = ManagerSelection.Instance.Character3 != null ? ManagerSelection.Instance.Character3.Model.Key : null;
        data.selectedCharacter4 = ManagerSelection.Instance.Character4 != null ? ManagerSelection.Instance.Character4.Model.Key : null;

        /*data.expRayMimi = gameObject.GetComponent<EarnManager>().expRayMimi;
		data.lifeRayMimi = gameObject.GetComponent<EarnManager>().life[0];
		data.damageRayMimi = gameObject.GetComponent<EarnManager>().damage[0];
		data.levelRayMimi = gameObject.GetComponent<EarnManager>().level[0];
		data.nextLevelExpRayMimi = gameObject.GetComponent<EarnManager>().nextLevelExpRayMimi;
		data.nextLevelCoinRayMimi = gameObject.GetComponent<EarnManager>().nextLevelCoinRayMimi;

		data.expFireMimi = gameObject.GetComponent<EarnManager>().expFireMimi;
		data.lifeFireMimi = gameObject.GetComponent<EarnManager>().life[1];
		data.damageFireMimi = gameObject.GetComponent<EarnManager>().damage[1];
		data.levelFireMimi = gameObject.GetComponent<EarnManager>().level[1];
		data.nextLevelExpFireMimi = gameObject.GetComponent<EarnManager>().nextLevelExpFireMimi;
		data.nextLevelCoinFireMimi = gameObject.GetComponent<EarnManager>().nextLevelCoinFireMimi;

		data.expPlantMimi = gameObject.GetComponent<EarnManager>().expPlantMimi;
		data.lifePlantMimi = gameObject.GetComponent<EarnManager>().life[2];
		data.damagePlantMimi = gameObject.GetComponent<EarnManager>().damage[2];
		data.levelPlantMimi = gameObject.GetComponent<EarnManager>().level[2];
		data.nextLevelExpPlantMimi = gameObject.GetComponent<EarnManager>().nextLevelExpPlantMimi;
		data.nextLevelCoinPlantMimi = gameObject.GetComponent<EarnManager>().nextLevelCoinPlantMimi;

		data.expWaterMimi = gameObject.GetComponent<EarnManager>().expWaterMimi;
		data.lifeWaterMimi = gameObject.GetComponent<EarnManager>().life[3];
		data.damageWaterMimi = gameObject.GetComponent<EarnManager>().damage[3];
		data.levelWaterMimi = gameObject.GetComponent<EarnManager>().level[3];
		data.nextLevelExpWaterMimi = gameObject.GetComponent<EarnManager>().nextLevelExpWaterMimi;
		data.nextLevelCoinWaterMimi = gameObject.GetComponent<EarnManager>().nextLevelCoinWaterMimi;

		data.expSpecialMimi = gameObject.GetComponent<EarnManager>().expSpecialMimi;
		data.lifeSpecialMimi = gameObject.GetComponent<EarnManager>().life[4];
		data.damageSpecialMimi = gameObject.GetComponent<EarnManager>().damage[4];
		data.levelSpecialMimi = gameObject.GetComponent<EarnManager>().level[4];
		data.nextLevelExpSpecialMimi = gameObject.GetComponent<EarnManager>().nextLevelExpSpecialMimi;
		data.nextLevelCoinSpecialMimi = gameObject.GetComponent<EarnManager>().nextLevelCoinSpecialMimi;*/

        data.locked = gameObject.GetComponent<EarnManager>().locked;

		bf.Serialize(file, data);
		file.Close();
	}

	public void Load(){
		if(File.Exists(Application.persistentDataPath + "/playerInfo.dat")){
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize(file);
			file.Close();

            //gameObject.GetComponent<EarnManager>().nextLevel = data.nextLevel;
            EarnManager.Instance.Coins = data.coins;
            EarnManager.Instance.screws = data.screws;
            EarnManager.Instance.nuts = data.nuts;

            EarnManager.Instance.characters.Clear();
            foreach (Character.SaveData character in data.characters)
            {
                EarnManager.Instance.characters.Add(character.Key, character);
            }

            ManagerSelection.Instance.Character1 = !String.IsNullOrEmpty(data.selectedCharacter1)
                    && CharactersManager.Instance.PlayableCharacters.ContainsKey(data.selectedCharacter1)
                    ? CharactersManager.Instance.PlayableCharacters[data.selectedCharacter1] : null;

            ManagerSelection.Instance.Character2 = !String.IsNullOrEmpty(data.selectedCharacter2)
                    && CharactersManager.Instance.PlayableCharacters.ContainsKey(data.selectedCharacter2)
                    ? CharactersManager.Instance.PlayableCharacters[data.selectedCharacter2] : null;

            ManagerSelection.Instance.Character3 = !String.IsNullOrEmpty(data.selectedCharacter3)
                    && CharactersManager.Instance.PlayableCharacters.ContainsKey(data.selectedCharacter3)
                    ? CharactersManager.Instance.PlayableCharacters[data.selectedCharacter3] : null;

            ManagerSelection.Instance.Character4 = !String.IsNullOrEmpty(data.selectedCharacter4)
                    && CharactersManager.Instance.PlayableCharacters.ContainsKey(data.selectedCharacter4)
                    ? CharactersManager.Instance.PlayableCharacters[data.selectedCharacter4] : null;

            /*gameObject.GetComponent<EarnManager>().expRayMimi = data.expRayMimi;
			gameObject.GetComponent<EarnManager>().life[0] = data.lifeRayMimi;
			gameObject.GetComponent<EarnManager>().damage[0] = data.damageRayMimi;
			gameObject.GetComponent<EarnManager>().level[0] = data.levelRayMimi;
			gameObject.GetComponent<EarnManager>().nextLevelExpRayMimi = data.nextLevelExpRayMimi;
			gameObject.GetComponent<EarnManager>().nextLevelCoinRayMimi = data.nextLevelCoinRayMimi;

			gameObject.GetComponent<EarnManager>().expFireMimi = data.expFireMimi;
			gameObject.GetComponent<EarnManager>().life[1] = data.lifeFireMimi;
			gameObject.GetComponent<EarnManager>().damage[1] = data.damageFireMimi;
			gameObject.GetComponent<EarnManager>().level[1] = data.levelFireMimi;
			gameObject.GetComponent<EarnManager>().nextLevelExpFireMimi = data.nextLevelExpFireMimi;
			gameObject.GetComponent<EarnManager>().nextLevelCoinFireMimi = data.nextLevelCoinFireMimi;

			gameObject.GetComponent<EarnManager>().expPlantMimi = data.expPlantMimi;
			gameObject.GetComponent<EarnManager>().life[2] = data.lifePlantMimi;
			gameObject.GetComponent<EarnManager>().damage[2] = data.damagePlantMimi;
			gameObject.GetComponent<EarnManager>().level[2] = data.levelPlantMimi;
			gameObject.GetComponent<EarnManager>().nextLevelExpPlantMimi = data.nextLevelExpPlantMimi;
			gameObject.GetComponent<EarnManager>().nextLevelCoinPlantMimi = data.nextLevelCoinPlantMimi;

			gameObject.GetComponent<EarnManager>().expWaterMimi = data.expWaterMimi;
			gameObject.GetComponent<EarnManager>().life[3] = data.lifeWaterMimi;
			gameObject.GetComponent<EarnManager>().damage[3] = data.damageWaterMimi;
			gameObject.GetComponent<EarnManager>().level[3] = data.levelWaterMimi;
			gameObject.GetComponent<EarnManager>().nextLevelExpWaterMimi = data.nextLevelExpWaterMimi;
			gameObject.GetComponent<EarnManager>().nextLevelCoinWaterMimi = data.nextLevelCoinWaterMimi;

			gameObject.GetComponent<EarnManager>().expSpecialMimi = data.expSpecialMimi;
			gameObject.GetComponent<EarnManager>().life[4] = data.lifeSpecialMimi;
			gameObject.GetComponent<EarnManager>().damage[4] = data.damageSpecialMimi;
			gameObject.GetComponent<EarnManager>().level[4] = data.levelSpecialMimi;
			gameObject.GetComponent<EarnManager>().nextLevelExpSpecialMimi = data.nextLevelExpSpecialMimi;
			gameObject.GetComponent<EarnManager>().nextLevelCoinSpecialMimi = data.nextLevelCoinSpecialMimi;*/

            //GameObject.Find("ManagerMenu").GetComponent<CoinManager>().coins = data.coins;

			//GameObject.Find("ManagerMenu").GetComponent<CoinManager>().earnCoins();

            EarnManager.Instance.locked = data.locked;
        }
		else{
			ResetGame();
		}

        CharactersManager.Instance.RefreshCharacters();
    }

	public void ResetGame(){
        Debug.Log("ResetGame");

		//BinaryFormatter bf = new BinaryFormatter();
		//FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.OpenOrCreate);
		//PlayerData data = (PlayerData)bf.Deserialize(file);
		//file.Close();

        EarnManager.Instance.Coins = 0;

        EarnManager.Instance.characters.Clear();

        Debug.Log(CharactersManager.Instance.PlayableCharacters.Count);
        foreach (Character character in CharactersManager.Instance.PlayableCharacters.Values)
        {
            if (character.Model.IsDefault)
            {
                EarnManager.Instance.characters.Add(character.Model.Key, new Character.SaveData(
                        character.Model.Key, CharactersManager.Instance.FirstLevel, CharactersManager.Instance.FirstLevelExperience, true));
            }
        }

        //gameObject.GetComponent<EarnManager>().nextLevel = 0;
        /*gameObject.GetComponent<EarnManager>().coins = 0;
		gameObject.GetComponent<EarnManager>().screws = 0;
		gameObject.GetComponent<EarnManager>().nuts = 0;

		gameObject.GetComponent<EarnManager>().expRayMimi = 0;
		gameObject.GetComponent<EarnManager>().life[0] = 900;
		gameObject.GetComponent<EarnManager>().damage[0] = 36;
		gameObject.GetComponent<EarnManager>().level[0] = 1;
		gameObject.GetComponent<EarnManager>().nextLevelExpRayMimi = 400;
		gameObject.GetComponent<EarnManager>().nextLevelCoinRayMimi = 100;

		gameObject.GetComponent<EarnManager>().expFireMimi = 0;
		gameObject.GetComponent<EarnManager>().life[1] = 1000;
		gameObject.GetComponent<EarnManager>().damage[1] = 28;
		gameObject.GetComponent<EarnManager>().level[1] = 1;
		gameObject.GetComponent<EarnManager>().nextLevelExpFireMimi = 400;
		gameObject.GetComponent<EarnManager>().nextLevelCoinFireMimi = 100;

		gameObject.GetComponent<EarnManager>().expPlantMimi = 0;
		gameObject.GetComponent<EarnManager>().life[2] = 700;
		gameObject.GetComponent<EarnManager>().damage[2] = 40;
		gameObject.GetComponent<EarnManager>().level[2] = 1;
		gameObject.GetComponent<EarnManager>().nextLevelExpPlantMimi = 400;
		gameObject.GetComponent<EarnManager>().nextLevelCoinPlantMimi = 100;

		gameObject.GetComponent<EarnManager>().expWaterMimi = 0;
		gameObject.GetComponent<EarnManager>().life[3] = 800;
		gameObject.GetComponent<EarnManager>().damage[3] = 32;
		gameObject.GetComponent<EarnManager>().level[3] = 1;
		gameObject.GetComponent<EarnManager>().nextLevelExpWaterMimi = 400;
		gameObject.GetComponent<EarnManager>().nextLevelCoinWaterMimi = 100;

		gameObject.GetComponent<EarnManager>().expSpecialMimi = 0;
		gameObject.GetComponent<EarnManager>().life[4] = 800;
		gameObject.GetComponent<EarnManager>().damage[4] = 32;
		gameObject.GetComponent<EarnManager>().level[4] = 1;
		gameObject.GetComponent<EarnManager>().nextLevelExpSpecialMimi = 400;
		gameObject.GetComponent<EarnManager>().nextLevelCoinSpecialMimi = 100;*/

        //GameObject.Find("ManagerMenu").GetComponent<CoinManager>().coins = 0;

		//GameObject.Find("ManagerMenu").GetComponent<CoinManager>().earnCoins();

        EarnManager.Instance.locked = true;

		Save();

		PlayerPrefs.DeleteAll();
	}
}

[Serializable]
class PlayerData{

	//public int nextLevel;
	public int coins;
	public int screws;
	public int nuts;
    public Character.SaveData[] characters;

    public string selectedCharacter1;
    public string selectedCharacter2;
    public string selectedCharacter3;
    public string selectedCharacter4;

    /*public int expRayMimi;
	public int lifeRayMimi;
	public int damageRayMimi;
	public int levelRayMimi;

	public int expFireMimi;
	public int lifeFireMimi;
	public int damageFireMimi;
	public int levelFireMimi;

	public int expPlantMimi;
	public int lifePlantMimi;
	public int damagePlantMimi;
	public int levelPlantMimi;

	public int expWaterMimi;
	public int lifeWaterMimi;
	public int damageWaterMimi;
	public int levelWaterMimi;

	public int expSpecialMimi;
	public int lifeSpecialMimi;
	public int damageSpecialMimi;
	public int levelSpecialMimi;

	public int nextLevelExpRayMimi = 400;
	public int nextLevelCoinRayMimi = 100;

	public int nextLevelExpFireMimi = 400;
	public int nextLevelCoinFireMimi = 100;

	public int nextLevelExpPlantMimi = 400;
	public int nextLevelCoinPlantMimi = 100;

	public int nextLevelExpWaterMimi = 400;
	public int nextLevelCoinWaterMimi = 100;

	public int nextLevelExpSpecialMimi = 400;
	public int nextLevelCoinSpecialMimi = 100;*/

    public bool locked = true;
}
