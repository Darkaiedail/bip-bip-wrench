﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

//[RequireComponent(typeof(HorizontalLayoutGroup))]
public class HorizontalScrollNavigationController : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler/*, IPointerDownHandler*/
{
    #region Unity Editor Members

    [SerializeField]
    float _relativeDragWidth = 120f;

    [SerializeField]
    float _scrollDuration = 0.5f;

    [SerializeField]
    float _scrollMinDuration = 0.1f;

    [SerializeField]
    float _scrollMaxDuration = 1.0f;

    [SerializeField]
    float _elasticity = 0.3f;

    [SerializeField]
    AnimationCurve _scrollCurve;

    [SerializeField]
    int _currentIndex;

    #endregion

    #region Private Members

    RectTransform _rectTransform;
    //HorizontalLayoutGroup _horizontalLayoutGroup;
    float _screenWidth;
    float _screenOffset;
    float[] _screenPositions;

    float _dragWidth;
    float _pointerOffset;
    bool _scrolling = false;
    int _desiredIndex;

    #endregion

    #region Public Properties

    public int CurrentIndex
    {
        get { return _currentIndex; }
        set {
            _currentIndex = value;
            _desiredIndex = _currentIndex;
            if (_rectTransform != null) {
                _rectTransform.anchoredPosition = new Vector2(_screenPositions[_currentIndex], 0);
            }
        }
    }

    #endregion

    #region MonoBehaviour

    void Awake()
    {
        _rectTransform = transform as RectTransform;
        //_horizontalLayoutGroup = GetComponent<HorizontalLayoutGroup>();
        _screenWidth = _rectTransform.sizeDelta.x / transform.childCount;

        float size = transform.root.GetComponent<CanvasScaler>().referenceResolution.x / (float)Screen.width;
        _screenOffset = (_screenWidth - (float)Screen.width * size) / 2;
        _screenPositions = new float[transform.childCount];

        _dragWidth = (Screen.width / _rectTransform.sizeDelta.x) * _relativeDragWidth;

        float screenPosition = 0;
        for (int i = 0; i < transform.childCount; ++i)
        {
            _screenPositions[i] = screenPosition - _screenOffset;
            screenPosition -= _screenWidth;
        }

        _desiredIndex = _currentIndex;
        _rectTransform.anchoredPosition = new Vector2(_screenPositions[_currentIndex], 0);
    }

    #endregion

    #region Pointer and Drag Events Handlers


    /*public void OnPointerDown(PointerEventData eventData)
    {
        _scrolling = false;
    }*/

    public void OnBeginDrag(PointerEventData eventData)
    {
        _scrolling = false;
        _pointerOffset = eventData.position.x;
    }

    /* void OnMouseDown()
    {
        _scrolling = false;
    }*/

    public void OnDrag(PointerEventData eventData)
    {
        float traslation = eventData.delta.x;
        if ((eventData.delta.x < 0 && _currentIndex == _screenPositions.Length - 1) || 
            (eventData.delta.x > 0 && _currentIndex == 0))
        {
            traslation *= _elasticity;
        }

        _rectTransform.anchoredPosition = new Vector2(_rectTransform.anchoredPosition.x + traslation, 0);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        float direction = eventData.position.x - _pointerOffset;
        if (direction > _dragWidth)
        {
            MovePrevious();
        } else if (direction < -_dragWidth) {
            MoveNext();
        } else {
            StartCoroutine(DoScroll(_screenPositions[_currentIndex]));
        }
    }

    #endregion

    #region Private Methods

    void MovePrevious()
    {
        if (_currentIndex > 0)
        {
            if (_desiredIndex < _currentIndex)
            {
                _desiredIndex = _currentIndex;
            }
            else
            {
                _desiredIndex = _currentIndex - 1;
            }
        }

        StartCoroutine(DoScroll(_screenPositions[_desiredIndex]));
    }

    void MoveNext()
    {
        if (_currentIndex < _screenPositions.Length - 1)
        {
            if (_desiredIndex > _currentIndex)
            {
                _desiredIndex = _currentIndex;
            }
            else
            {
                _desiredIndex = _currentIndex + 1;
            }
        }
        StartCoroutine(DoScroll(_screenPositions[_desiredIndex]));
    }

    IEnumerator DoScroll(float scrollTo)
    {
        int fromIndex = _currentIndex;
        int toIndex = _desiredIndex;

        _scrolling = true;

        if (OnBeginScrollNavigation != null) OnBeginScrollNavigation(this, fromIndex, toIndex);

        float scrollFrom = _rectTransform.anchoredPosition.x;
        float scroll = scrollTo - scrollFrom;

        float duration = Mathf.Clamp(Mathf.Abs(scroll / _screenWidth) * _scrollDuration, _scrollMinDuration, _scrollMaxDuration);
        float currentScrollTime = 0;

        while (_scrolling && currentScrollTime < 1 && !Mathf.Approximately(_rectTransform.anchoredPosition.x, scrollTo)) {
            /*if (!_scrolling)
            {
                yield return new WaitForFixedUpdate();
            }*/

            currentScrollTime = Mathf.Clamp01(currentScrollTime + Time.deltaTime / duration);

            float traslation = _scrollCurve.Evaluate(currentScrollTime) * scroll;
            _rectTransform.anchoredPosition = new Vector2(scrollFrom + traslation, 0);
            yield return new WaitForFixedUpdate();
        }

        _rectTransform.anchoredPosition = new Vector2(scrollTo, 0);
        _currentIndex = _desiredIndex;
        _scrolling = false;

        if (OnEndScrollNavigation != null) OnEndScrollNavigation(this, fromIndex, toIndex);

        yield return null;
    }

    #endregion

    #region Public Methods

    public void ScrollTo(int index)
    {
        _desiredIndex = index;
        StartCoroutine(DoScroll(_screenPositions[_desiredIndex]));
    }

    #endregion

    #region Events

    public delegate void ScrollNavigationEventHandler(HorizontalScrollNavigationController sender, int fromIndex, int toIndex);
    public event ScrollNavigationEventHandler OnEndScrollNavigation;
    public event ScrollNavigationEventHandler OnBeginScrollNavigation;

    #endregion
}
