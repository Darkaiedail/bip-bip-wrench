﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent(typeof(Button))]
public class CharacterCardButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    #region Unity Editor Members

    [SerializeField]
    float _holdTime = 0.5f;

    #endregion

    #region Private Members

    Character _character;

    bool _buttonHeld;
    float _holdingBeginsTime;

    Button _button;

    #endregion

    #region Public Properties

    public Character Character
    {
        get
        {
            return _character;
        }

        set
        {
            _character = value;
        }
    }

    #endregion

    #region MonoBehaviour Methods

    public void Start()
    {
        _button = GetComponent<Button>();
    }

    #endregion

    #region Pointer Events 

    public void OnPointerDown(PointerEventData eventData)
    {
        if (_button.interactable)
        {
            _buttonHeld = true;
            _holdingBeginsTime = Time.time;
            StartCoroutine(Holding());
        }
    }
	
    public void OnPointerUp(PointerEventData eventData)
    {
        _buttonHeld = false;
    }

    #endregion

    #region Coroutines Methods

    IEnumerator Holding()
    {
        while (Time.time < _holdingBeginsTime + _holdTime) {
            yield return new WaitForFixedUpdate();

            if (!_buttonHeld) yield break;
        }

        MenuUIController.Instance.ShowCharacterCard(_character);
        _buttonHeld = false;
        yield return null;
    }

    #endregion
}
