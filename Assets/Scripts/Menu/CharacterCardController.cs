﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class CharacterCardController : MonoBehaviour
{
    #region Unity Editor

    [SerializeField]
    Text _nameText;

    [SerializeField]
    Text _descriptionText;

    [SerializeField]
    Image _imageBackground;

    [SerializeField]
    Image _imageBox;

    [SerializeField]
    Text _levelText;

    [SerializeField]
    Text _experienceText;

    [SerializeField]
    Slider _experienceSlider;

    [SerializeField]
    Button _levelUpButton;

    [SerializeField]
    Text _levelUpCostText;

    [SerializeField]
    Text _healthText;

    [SerializeField]
    Slider _healthSlider;

    [SerializeField]
    Slider _healthLevelUpSlider;

    [SerializeField]
    Text _healthLevelUpText;

    [SerializeField]
    Text _damageText;

    [SerializeField]
    Slider _damageSlider;

    [SerializeField]
    Slider _damageLevelUpSlider;

    [SerializeField]
    Text _damageLevelUpText;

    [SerializeField]
    Text _reparationText;

    [SerializeField]
    Slider _reparationSlider;

    [SerializeField]
    Slider _reparationLevelUpSlider;

    [SerializeField]
    Text _reparationLevelUpText;

    //Thinking in the future

    //[SerializeField]
    //Text _tauntText;

    //[SerializeField]
    //Slider _tauntSlider;

    //[SerializeField]
    //Slider _tauntLevelUpSlider;

    //[SerializeField]
    //Slider _tauntLevelUpText;

    [SerializeField]
    Button _purchaseButton;

    [SerializeField]
    Text _purchaseCostText;

    //TODO: Change by Sprites
    [SerializeField]
    Color _rayBackgroundColor;

    [SerializeField]
    Color _fireBackgroundColor;

    [SerializeField]
    Color _plantBackgroundColor;

    [SerializeField]
    Color _waterBackgroundColor;

    #endregion

    #region Private members

    Character _character;

    #endregion

    public void Load(Character character)
    {
        _healthSlider.maxValue = CharactersManager.Instance.MaxHealth;
        _healthLevelUpSlider.maxValue = CharactersManager.Instance.MaxHealth;

        _damageSlider.maxValue = CharactersManager.Instance.MaxDamage;
        _damageLevelUpSlider.maxValue = CharactersManager.Instance.MaxDamage;

        _reparationSlider.maxValue = CharactersManager.Instance.MaxReparation;
        _reparationLevelUpSlider.maxValue = CharactersManager.Instance.MaxReparation;

        //System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.GetCultureInfo(SmartLocalization.LanguageManager.Instance.CurrentlyLoadedCulture.languageCode);

        _character = character;

        _nameText.text = _character.Model.GetName();
        _descriptionText.text = _character.Model.GetDescription();

        _imageBackground.color = GetElementColor(_character.Model.Element);
        _imageBox.sprite = _character.Model.Image;

        _levelText.text = String.Format(SmartLocalization.LanguageManager.Instance.GetTextValue("Lvl"), _character.Level);



        if (_character.Level < CharactersManager.Instance.LastLevel)
        {
            int nextLevel = _character.Level + 1;
            float currentLevelExperience = CharactersManager.Instance.GetLevelExperience(_character.Level);
            float nextLevelExperience = CharactersManager.Instance.GetLevelExperience(nextLevel);

            _experienceText.text = String.Format(SmartLocalization.LanguageManager.Instance.GetTextValue("Exp"), _character.TotalExperience, nextLevelExperience);

            _levelUpButton.gameObject.SetActive(_character.CanLevelUp);

            _healthLevelUpSlider.gameObject.SetActive(_character.CanLevelUp);
            _healthLevelUpText.transform.parent.gameObject.SetActive(_character.CanLevelUp);
            
            _damageLevelUpSlider.gameObject.SetActive(_character.CanLevelUp);
            _damageLevelUpText.transform.parent.gameObject.SetActive(_character.CanLevelUp);

            _reparationLevelUpSlider.gameObject.SetActive(_character.CanLevelUp);
            _reparationLevelUpText.transform.parent.gameObject.SetActive(_character.CanLevelUp);

            //_tauntLevelUpSlider.gameObject.SetActive(canLevelUp);
            //_tauntLevelUpText.gameObject.transform.parent.SetActive(canLevelUp);

            if (_character.CanLevelUp)
            {
                float levelExperience = nextLevelExperience - currentLevelExperience;
                _experienceSlider.maxValue = levelExperience;
                _experienceSlider.value = levelExperience;

                _levelUpCostText.text = _character.LevelUpPrice.ToString();

                int nextLevelHealth = (int)_character.Model.GetBaseHealth(nextLevel);
                _healthLevelUpText.text = String.Format("+{0}", nextLevelHealth - (int)_character.BaseHealth);
                _healthLevelUpSlider.value = nextLevelHealth;

                int nextLevelDamage = (int)_character.Model.GetBaseDamage(nextLevel);
                _damageLevelUpText.text = String.Format("+{0}", nextLevelDamage - (int)_character.BaseDamage);
                _damageLevelUpSlider.value = nextLevelDamage;

                int nextLevelReparation = (int)_character.Model.GetBaseReparation(nextLevel);
                _reparationLevelUpText.text = String.Format("+{0}", nextLevelReparation - (int)_character.BaseReparation);
                _reparationLevelUpSlider.value = nextLevelReparation;

                //int nextLevelTaunt = (int)_character.Model.GetBaseTaunt(nextLevel);
                //_tauntLevelUpText.text = String.Format("+{0}", (int)(nextLevelTaunt - _character.BaseTaunt));
                //_tauntLevelUpSlider.value = nextLevelTaunt;
            }
            else
            {
                _experienceSlider.maxValue = nextLevelExperience - currentLevelExperience;
                _experienceSlider.value = _experienceSlider.maxValue - _character.ExperienceToNextLevel;
            }
        }
        else
        {


            _experienceText.text = String.Format(SmartLocalization.LanguageManager.Instance.GetTextValue("Exp"), CharactersManager.Instance.LastLevelExperience, CharactersManager.Instance.LastLevelExperience);

            _experienceSlider.maxValue = 1f;
            _experienceSlider.value = 1f;
            //TODO: Make sexy effects
        }

        _healthText.text = ((int)_character.BaseHealth).ToString();
        _healthSlider.value = _character.BaseHealth;

        _damageText.text = ((int)_character.BaseDamage).ToString();
        _damageSlider.value = _character.BaseDamage;

        _reparationText.text = ((int)_character.BaseReparation).ToString();
        _reparationSlider.value = _character.BaseReparation;

        //_tauntText.text = _character.BaseTaunt.ToString("N");
        //_tauntSlider.value = _character.BaseTaunt;

        _purchaseButton.gameObject.SetActive(!_character.Owned);
        _purchaseCostText.text = _character.Model.Price.ToString();


    }

    //TODO: Change By GetElementBackgroundSprite
    Color GetElementColor(CharacterModel.CharacterElement element)
    {
        switch (element)
        {
            case CharacterModel.CharacterElement.Ray:
                return _rayBackgroundColor;

            case CharacterModel.CharacterElement.Fire:
                return _fireBackgroundColor;

            case CharacterModel.CharacterElement.Plant:
                return _plantBackgroundColor;

            case CharacterModel.CharacterElement.Water:
                return _waterBackgroundColor;
        }

        return Color.white;
    }

    public void Buy()
    {
        if (!_character.Owned && EarnManager.Instance.Coins >= _character.Model.Price)
        {
            EarnManager.Instance.Coins -= _character.Model.Price;
            _character.Owned = true;

            Load(_character);

            EarnManager.Instance.UpdateCharacters();
            SaveManager.Instance.Save();

            if (OnCharacterChangeChange != null)
            {
                OnCharacterChangeChange(_character);
            }
        }
    }

    public void LevelUp()
    {
        if (_character.CanLevelUp && EarnManager.Instance.Coins >= _character.LevelUpPrice)
        {
            EarnManager.Instance.Coins -= _character.LevelUpPrice;
            ++_character.Level;

            Load(_character);

            EarnManager.Instance.UpdateCharacters();
            SaveManager.Instance.Save();

            if (OnCharacterChangeChange != null)
            {
                OnCharacterChangeChange(_character);
            }
        }
    }

    #region Events

    //TODO: Move from heare
    public delegate void OnCharacterChangeEventHandler(Character character);
    public static event OnCharacterChangeEventHandler OnCharacterChangeChange;

    #endregion
}
