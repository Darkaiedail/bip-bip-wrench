﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(LevelBriefingController))]
public class SelectedCharactersPanelController : CharactersPanelController {

    #region Unity Editor members

    //[SerializeField]
    //GridLayoutGroup _adquiredCharacters;

    //[SerializeField]
    //GridLayoutGroup _promotionCharacters;

    [Header("Selection")]

    [SerializeField]
    Image _selectedCard1Box;

    [SerializeField]
    Image _selectedCard2Box;

    [SerializeField]
    Image _selectedCard3Box;

    [SerializeField]
    Image _selectedCard4Box;

    [SerializeField]
    Image _selectedCardSocialBox;

    [Header("Totals")]

    [SerializeField]
    Text _healthText;

    [SerializeField]
    Slider _healthSlider;

    [SerializeField]
    Text _damageText;

    [SerializeField]
    Slider _damageSlider;

    [SerializeField]
    Text _reparationText;

    [SerializeField]
    Slider _reparationSlider;

    //Thinking in the future

    //[SerializeField]
    //Text _tauntText;

    //[SerializeField]
    //Slider _tauntSlider;

    #endregion

    LevelBriefingController _levelBriefingController;

    Dictionary<Character, Image> _cardsImages = new Dictionary<Character, Image>();

    Dictionary<Image, Character> _selectedCharacters = new Dictionary<Image, Character>();
    
    public int CountSelectedCharacters {
        get { return _selectedCharacters.Count;  }
    }

    void Start()
    {
        _levelBriefingController = GetComponent<LevelBriefingController>();

        _healthSlider.maxValue = CharactersManager.Instance.MaxHealth * 5;
        _damageSlider.maxValue = CharactersManager.Instance.MaxDamage * 5;
        _reparationSlider.maxValue = CharactersManager.Instance.MaxReparation * 5;
        //_tauntSlider.maxValue = CharactersManager.Instance.MaxTaunt * 5;

        foreach (Character character in CharactersManager.Instance.PlayableCharacters.Values)
        {
            if (character.Owned)
            {
                CharacterCardButton characterCardButton = Instantiate(_characterCardButtonPrefab);
                characterCardButton.Character = character;

                Image cardImage = characterCardButton.transform.FindChild("ThumbnailImage").GetComponent<Image>();
                cardImage.sprite = character.Model.ThumbnailImage;

                characterCardButton.GetComponent<Image>().color = GetElementColor(character.Model.Element);

                _cardsImages.Add(character, cardImage);
                characterCardButton.transform.SetParent(_adquiredCharacters.transform, false);

                Character selectCharacter = character;
                characterCardButton.GetComponent<Button>().onClick.AddListener(() => SelectCard(selectCharacter));
            }
        }

        LoadData();

        CharacterCardController.OnCharacterChangeChange += CharacterChanged;
    }

    private void CharacterChanged(Character character)
    {
        if (character.Owned && !_cardsImages.ContainsKey(character))
        {
            CharacterCardButton characterCardButton = Instantiate(_characterCardButtonPrefab);
            characterCardButton.Character = character;

            Image cardImage = characterCardButton.transform.FindChild("ThumbnailImage").GetComponent<Image>();
            cardImage.sprite = character.Model.ThumbnailImage;

            characterCardButton.GetComponent<Image>().color = GetElementColor(character.Model.Element);

            _cardsImages.Add(character, cardImage);
            characterCardButton.transform.SetParent(_adquiredCharacters.transform, false);

            Character selectCharacter = character;
            characterCardButton.GetComponent<Button>().onClick.AddListener(() => SelectCard(selectCharacter));
        }

        LoadData();
    }


    public void SelectCard(Character character)
    {
        Image cardImage = _cardsImages[character];
        Image emptySelectedCard = null;

        if (!_selectedCharacters.ContainsValue(character)) {
            if (_selectedCard1Box.sprite == null)
            {
                emptySelectedCard = _selectedCard1Box;
            }
            else if (_selectedCard2Box.sprite == null)
            {
                emptySelectedCard = _selectedCard2Box;
            }
            else if (_selectedCard3Box.sprite == null)
            {
                emptySelectedCard = _selectedCard3Box;
            }
            else if (_selectedCard4Box.sprite == null)
            {
                emptySelectedCard = _selectedCard4Box;
            }

            if (emptySelectedCard != null)
            {
                emptySelectedCard.sprite = character.Model.ThumbnailImage;
                emptySelectedCard.color = Color.white;

                _selectedCharacters.Add(emptySelectedCard, character);

                emptySelectedCard.transform.parent.GetComponent<CharacterCardButton>().Character = character;
                emptySelectedCard.transform.parent.GetComponent<Button>().interactable = true;
                emptySelectedCard.transform.parent.GetComponent<Image>().color = cardImage.transform.parent.GetComponent<Image>().color;

                emptySelectedCard.transform.parent.FindChild("DetailsImage").gameObject.SetActive(true);

                cardImage.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
                cardImage.transform.parent.GetComponent<Button>().interactable = false;
                cardImage.transform.parent.FindChild("DetailsImage").gameObject.SetActive(false);

            } else {
                //TODO: Notify
                Debug.Log("All boxes are full");
            }
        } else
        {
            //TODO: Notify

            Debug.Log("Character yet selected");
        }
    }

    public void UnselectCard(Image selectedCardImage)
    {
        Character character = _selectedCharacters[selectedCardImage];
        Image cardImage = _cardsImages[character];

        selectedCardImage.sprite = null;
        selectedCardImage.color = new Color(1f, 1f, 1f, 0);

        selectedCardImage.transform.parent.GetComponent<CharacterCardButton>().Character = null;
        selectedCardImage.transform.parent.GetComponent<Button>().interactable = false;
        selectedCardImage.transform.parent.GetComponent<Image>().color = Color.white;
        selectedCardImage.transform.parent.FindChild("DetailsImage").gameObject.SetActive(false);

        _selectedCharacters.Remove(selectedCardImage);

#if UNITY_EDITOR
        //This cannot be null
        if (cardImage == null)
        {
            Debug.LogError("[SelectedCharactersController] Card Image cannot be null.");
            return;
        }
#endif
        cardImage.color = Color.white;
        cardImage.transform.parent.GetComponent<Button>().interactable = true;
        cardImage.transform.parent.FindChild("DetailsImage").gameObject.SetActive(true);
    }

    public void LoadData()
    {
        /*if (ManagerSelection.Instance.Character1 != null)
        {
            SelectCard(ManagerSelection.Instance.Character1);
            _finalCard1Box.sprite = ManagerSelection.Instance.Character1.Model.ThumbnailImage;
            _finalCard1Box.color = Color.white;
            _finalCard1Box.transform.parent.GetComponent<Image>().color = GetElementColor(ManagerSelection.Instance.Character1.Model.Element);

            _selectedCard1Box.sprite = ManagerSelection.Instance.Character1.Model.ThumbnailImage;
            _selectedCard1Box.color = Color.white;
            _selectedCard1Box.transform.parent.GetComponent<Image>().color = GetElementColor(ManagerSelection.Instance.Character1.Model.Element);
        }
        else
        {
            _finalCard1Box.color = new Color(1f, 1f, 1f, 0);
        }

        if (ManagerSelection.Instance.Character2 != null)
        {
            _finalCard2Box.sprite = ManagerSelection.Instance.Character2.Model.ThumbnailImage;
            _finalCard2Box.color = Color.white;
            _finalCard2Box.transform.parent.GetComponent<Image>().color = GetElementColor(ManagerSelection.Instance.Character2.Model.Element);

            _selectedCard2Box.sprite = ManagerSelection.Instance.Character2.Model.ThumbnailImage;
            _selectedCard2Box.color = Color.white;
            _selectedCard2Box.transform.parent.GetComponent<Image>().color = GetElementColor(ManagerSelection.Instance.Character2.Model.Element);
        }
        else
        {
            _finalCard2Box.color = new Color(1f, 1f, 1f, 0);
        }

        if (ManagerSelection.Instance.Character3 != null)
        {
            _finalCard3Box.sprite = ManagerSelection.Instance.Character3.Model.ThumbnailImage;
            _finalCard3Box.color = Color.white;
            _finalCard3Box.transform.parent.GetComponent<Image>().color = GetElementColor(ManagerSelection.Instance.Character3.Model.Element);

            _selectedCard3Box.sprite = ManagerSelection.Instance.Character3.Model.ThumbnailImage;
            _selectedCard3Box.color = Color.white;
            _selectedCard3Box.transform.parent.GetComponent<Image>().color = GetElementColor(ManagerSelection.Instance.Character3.Model.Element);
        }
        else
        {
            _finalCard3Box.color = new Color(1f, 1f, 1f, 0);
        }

        if (ManagerSelection.Instance.Character4 != null)
        {
            _finalCard4Box.sprite = ManagerSelection.Instance.Character4.Model.ThumbnailImage;
            _finalCard4Box.color = Color.white;
            _finalCard4Box.transform.parent.GetComponent<Image>().color = GetElementColor(ManagerSelection.Instance.Character4.Model.Element);

            _selectedCard4Box.sprite = ManagerSelection.Instance.Character4.Model.ThumbnailImage;
            _selectedCard4Box.color = Color.white;
            _selectedCard4Box.transform.parent.GetComponent<Image>().color = GetElementColor(ManagerSelection.Instance.Character4.Model.Element);
        }
        else
        {
            _finalCard4Box.color = new Color(1f, 1f, 1f, 0);
        }*/

        //TODO: check owned and dies

        if (ManagerSelection.Instance.Character1 != null)
        {
            SelectCard(ManagerSelection.Instance.Character1);
        }
        if (ManagerSelection.Instance.Character2 != null)
        {
            SelectCard(ManagerSelection.Instance.Character2);
        }
        if (ManagerSelection.Instance.Character3 != null)
        {
            SelectCard(ManagerSelection.Instance.Character3);
        }
        if (ManagerSelection.Instance.Character4 != null)
        {
            SelectCard(ManagerSelection.Instance.Character4);
        }

        ApplyChanges();
    }
    public void ApplyChanges()
    {
        ManagerSelection.Instance.Character1 = GetCharacter1();
        ManagerSelection.Instance.Character2 = GetCharacter2();
        ManagerSelection.Instance.Character3 = GetCharacter3();
        ManagerSelection.Instance.Character4 = GetCharacter4();
        ManagerSelection.Instance.CharacterSocial = GetCharacterSocial();

        _levelBriefingController.FinalCard1Box.transform.parent.GetComponent<CharacterCardButton>().Character = ManagerSelection.Instance.Character1;
        _levelBriefingController.FinalCard1Box.sprite = _selectedCard1Box.sprite;
        _levelBriefingController.FinalCard1Box.color = _selectedCard1Box.color;
        _levelBriefingController.FinalCard1Box.transform.parent.GetComponent<Image>().color = _selectedCard1Box.transform.parent.GetComponent<Image>().color;
        _levelBriefingController.FinalCard1Box.transform.parent.FindChild("DetailsImage").gameObject.SetActive(ManagerSelection.Instance.Character1 != null);

        _levelBriefingController.FinalCard2Box.transform.parent.GetComponent<CharacterCardButton>().Character = ManagerSelection.Instance.Character2;
        _levelBriefingController.FinalCard2Box.sprite = _selectedCard2Box.sprite;
        _levelBriefingController.FinalCard2Box.color = _selectedCard2Box.color;
        _levelBriefingController.FinalCard2Box.transform.parent.GetComponent<Image>().color = _selectedCard2Box.transform.parent.GetComponent<Image>().color;
        _levelBriefingController.FinalCard2Box.transform.parent.FindChild("DetailsImage").gameObject.SetActive(ManagerSelection.Instance.Character2 != null);

        _levelBriefingController.FinalCard3Box.transform.parent.GetComponent<CharacterCardButton>().Character = ManagerSelection.Instance.Character3;
        _levelBriefingController.FinalCard3Box.sprite = _selectedCard3Box.sprite;
        _levelBriefingController.FinalCard3Box.color = _selectedCard3Box.color;
        _levelBriefingController.FinalCard3Box.transform.parent.GetComponent<Image>().color = _selectedCard3Box.transform.parent.GetComponent<Image>().color;
        _levelBriefingController.FinalCard3Box.transform.parent.FindChild("DetailsImage").gameObject.SetActive(ManagerSelection.Instance.Character3 != null);

        _levelBriefingController.FinalCard4Box.transform.parent.GetComponent<CharacterCardButton>().Character = ManagerSelection.Instance.Character4;
        _levelBriefingController.FinalCard4Box.sprite = _selectedCard4Box.sprite;
        _levelBriefingController.FinalCard4Box.color = _selectedCard4Box.color;
        _levelBriefingController.FinalCard4Box.transform.parent.GetComponent<Image>().color = _selectedCard4Box.transform.parent.GetComponent<Image>().color;
        _levelBriefingController.FinalCard4Box.transform.parent.FindChild("DetailsImage").gameObject.SetActive(ManagerSelection.Instance.Character4 != null);

        //MLR - INFO: This is not the place to change this
        //_finalCardSocialBox.transform.parent.GetComponent<CharacterCardButton>().Character = ManagerSelection.Instance.CharacterSocial;
        //_finalCardSocialBox.sprite = _selectedCardSocialBox.sprite;
        //_finalCardSocialBox.color = _selectedCardSocialBox.color;
        //_finalCardSocialBox.transform.parent.GetComponent<Image>().color = _selectedCardSocialBox.transform.parent.GetComponent<Image>().color;
        //_levelBriefingController.FinalCardSocialBox.transform.parent.FindChild("DetailsImage").gameObject.SetActive(ManagerSelection.Instance.CharacterSocial != null);

        SaveManager.Instance.Save();

        ShowTotals();
    }

    void ShowTotals()
    {
        _healthSlider.value = 0;
        _damageSlider.value = 0;
        _reparationSlider.value = 0;
        //_tauntSlider.value = 0;

        if (ManagerSelection.Instance.Character1 != null)
        {
            _healthSlider.value += ManagerSelection.Instance.Character1.BaseHealth;
            _damageSlider.value += ManagerSelection.Instance.Character1.BaseDamage;
            _reparationSlider.value += ManagerSelection.Instance.Character1.BaseReparation;
            //_tauntSlider.value += ManagerSelection.Instance.Character1.Basetaunt;
        }
        if (ManagerSelection.Instance.Character2 != null)
        {
            _healthSlider.value += ManagerSelection.Instance.Character2.BaseHealth;
            _damageSlider.value += ManagerSelection.Instance.Character2.BaseDamage;
            _reparationSlider.value += ManagerSelection.Instance.Character2.BaseReparation;
            //_tauntSlider.value += ManagerSelection.Instance.Character2Basetaunt;
        }
        if (ManagerSelection.Instance.Character3 != null)
        {
            _healthSlider.value += ManagerSelection.Instance.Character3.BaseHealth;
            _damageSlider.value += ManagerSelection.Instance.Character3.BaseDamage;
            _reparationSlider.value += ManagerSelection.Instance.Character3.BaseReparation;
            //_tauntSlider.value += ManagerSelection.Instance.Character3.Basetaunt;
        }
        if (ManagerSelection.Instance.Character4 != null)
        {
            _healthSlider.value += ManagerSelection.Instance.Character4.BaseHealth;
            _damageSlider.value += ManagerSelection.Instance.Character4.BaseDamage;
            _reparationSlider.value += ManagerSelection.Instance.Character4.BaseReparation;
            //_tauntSlider.value += ManagerSelection.Instance.Character4.Basetaunt;
        }
        if (ManagerSelection.Instance.CharacterSocial != null)
        {
            _healthSlider.value += ManagerSelection.Instance.CharacterSocial.BaseHealth;
            _damageSlider.value += ManagerSelection.Instance.CharacterSocial.BaseDamage;
            _reparationSlider.value += ManagerSelection.Instance.CharacterSocial.BaseReparation;
            //_tauntSlider.value += ManagerSelection.Instance.CharacterSocial.Basetaunt;
        }

        _healthText.text = ((int)_healthSlider.value).ToString();
        _damageText.text = ((int)_damageSlider.value).ToString();
        _reparationText.text = ((int)_reparationSlider.value).ToString();
        //_tauntText.text = _tauntSlider.value.ToString();
    }

    public Character GetCharacter1()
    {
        return _selectedCharacters.ContainsKey(_selectedCard1Box) ? _selectedCharacters[_selectedCard1Box] : null;
    }

    public Character GetCharacter2()
    {
        return _selectedCharacters.ContainsKey(_selectedCard2Box) ? _selectedCharacters[_selectedCard2Box] : null;
    }

    public Character GetCharacter3()
    {
        return _selectedCharacters.ContainsKey(_selectedCard3Box) ? _selectedCharacters[_selectedCard3Box] : null;
    }

    public Character GetCharacter4()
    {
        return _selectedCharacters.ContainsKey(_selectedCard4Box) ? _selectedCharacters[_selectedCard4Box] : null;
    }

    public Character GetCharacterSocial()
    {
        return _selectedCharacters.ContainsKey(_selectedCardSocialBox) ? _selectedCharacters[_selectedCardSocialBox] : null;
    }
}
