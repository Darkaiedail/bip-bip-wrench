﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class MenuUIController : UIController<MenuUIController> {

    #region Constants & Enums

    public enum Screen
    {
        Inventory,
        Characters,
        SelectLevel,
        Social,
        Shop,
        LevelBriefing
    }

    #endregion

    #region Private Members

    static Screen _menuScreen = Screen.SelectLevel;

    SelectedCharactersPanelController _selectedCharactersController;
    CharacterCardController _characterCardController;

    RectTransform _selectedNavigationButton;

    #endregion

    #region Public Properties

    public static Screen MenuScreen
    {
        get { return _menuScreen; }
        set { _menuScreen = value; }
    }

    #endregion

    #region Unity Editor Members



    [Header("Menu Screen")]

    //[SerializeField]
    //Animator[] _navigationButtonsAnimators;

    [SerializeField]
    RectTransform[] _navigationButtons;

    [SerializeField]
    float _navigationSelectTime = 0.25f;

    [SerializeField]
    float _navigationUnselectTime = 0.25f;

    [SerializeField]
    float _navigationUnselectedScale = 0.75f;

    [SerializeField]
    HorizontalScrollNavigationController _navigationController;

    [SerializeField]
    CanvasGroup _menuCanvasGroup;

    [Header("Resources Bar")]
    [SerializeField]
    Text _softMoneyText;

    [Header("Level Briefing Screen")]

    [SerializeField]
    CanvasGroup _levelBriefingCanvasGroup;

    [SerializeField]
    CanvasGroup _levelSelectTeamCanvasGroup;

    [Header("Character Card Screen")]

    [SerializeField]
    CanvasGroup _characterCardCanvasGroup;

    #endregion;    

    #region MonoBehaviour

    void Awake()
    {
        _selectedCharactersController = _levelBriefingCanvasGroup.GetComponent<SelectedCharactersPanelController>();
        _characterCardController = _characterCardCanvasGroup.GetComponent<CharacterCardController>();

        _navigationController.OnBeginScrollNavigation += ChangeNavigationButton;
        //_navigationController.OnEndScrollNavigation += ChangeNavigationButton;

        //Starting values
        SoftMoneyChanged(EarnManager.Instance.Coins);

        //On Change Values
        EarnManager.Instance.OnSoftMoneyChange += SoftMoneyChanged;
    }

    void Start()
    {
        //_navigationButtonsAnimators[(int)_menuScreen].SetBool("Active", true);ç


        switch (_menuScreen)
        {
            case Screen.Inventory:

            case Screen.Characters:

            case Screen.SelectLevel:

            case Screen.Social:

            case Screen.Shop:
                _navigationController.CurrentIndex = (int)_menuScreen;
                GoToMenu();
                break;

            case Screen.LevelBriefing:
                GoToSelectLevel(EarnManager.Instance.SceneLevel);
                break;

            default:
                GoToMenu();
                break;
        }

        for (int i = 0; i < _navigationButtons.Length; ++i)
        {
            if (i != _navigationController.CurrentIndex)
            {
                _navigationButtons[i].localScale = new Vector3(_navigationUnselectedScale, _navigationUnselectedScale);
            }
        }

        _selectedNavigationButton = _navigationButtons[_navigationController.CurrentIndex];
    }

    #endregion

    #region Public Methods

    public void SoftMoneyChanged(int value)
    {
        _softMoneyText.text = String.Format("x {0}", value);
    }

    public void GoToMenu()
    {
        StartCoroutine(GoToCanvas(_menuCanvasGroup));
    }

    public void GoToSelectLevel(string level)
    {
        EarnManager.Instance.SceneLevel = level;
        StartCoroutine(GoToCanvas(_levelBriefingCanvasGroup));
    }

    public void ShowSelectTeam()
    {
        StartCoroutine(ShowCanvas(_levelSelectTeamCanvasGroup));
    }

    public void HideSelectTeam()
    {
        _selectedCharactersController.ApplyChanges();
        StartCoroutine(HideCanvas(_levelSelectTeamCanvasGroup));
    }

    public void ShowCharacterCard(Character character)
    {
        _characterCardController.Load(character);
        StartCoroutine(ShowCanvas(_characterCardCanvasGroup));
    }

    public void HideCharacterCard()
    {
        StartCoroutine(HideCanvas(_characterCardCanvasGroup));
    }

    public void NavigateTo(int index)
    {
        //_navigationButtonsAnimators[_navigationController.CurrentIndex].SetBool("Active", false);
        //_navigationButtonsAnimators[index].SetBool("Active", true);
        _navigationController.ScrollTo(index);

        
    }

    public void StartGame()
    {
        if (_selectedCharactersController.CountSelectedCharacters == 4)
        {
            /*ManagerSelection.Instance.i = _selectedCharactersController.SelectedCards[0];
            if (_selectedCharactersController.SelectedCards.Count > 1)
            {
                ManagerSelection.Instance.j = _selectedCharactersController.SelectedCards[1];
                if (_selectedCharactersController.SelectedCards.Count > 2)
                {
                    ManagerSelection.Instance.k = _selectedCharactersController.SelectedCards[2];
                    if (_selectedCharactersController.SelectedCards.Count > 3)
                    {
                        ManagerSelection.Instance.h = _selectedCharactersController.SelectedCards[3];

                        //Special Character
                        if (_selectedCharactersController.SelectedCards.Count > 4)
                        {
                            ManagerSelection.Instance. = _selectedCharactersController.SelectedCards[4];
                        }
                    }
                }*/

            ScenesManager.Instance.LoadScene(ScenesManager.GAME_SCENE);
        }
        else
        {
            //TODO: Notify
        }
    }

    private void ChangeNavigationButton(HorizontalScrollNavigationController sender, int fromIndex, int toIndex)
    {
        if (_navigationButtons[toIndex] != _selectedNavigationButton)
        {
            StartCoroutine(DoUnselectNavigationButton(_selectedNavigationButton));
            StartCoroutine(DoSelectNavigationButton(_navigationButtons[toIndex]));

            _selectedNavigationButton = _navigationButtons[toIndex];
        }
    }

    IEnumerator DoSelectNavigationButton(RectTransform buttonTransform)
    {
        float distance = 1 - _navigationUnselectedScale;
        float speed = distance / _navigationSelectTime;

        float scale = buttonTransform.localScale.x;

        while (buttonTransform.localScale.x < 1)
        {
            scale = Mathf.Clamp(scale + Time.deltaTime * speed, _navigationUnselectedScale, 1);

            buttonTransform.localScale = new Vector2(scale, scale);
            yield return new WaitForFixedUpdate();
        }

        buttonTransform.localScale = new Vector2(1, 1);

        yield return null;
    }

    IEnumerator DoUnselectNavigationButton(RectTransform buttonTransform)
    {
        float distance = 1 - _navigationUnselectedScale;
        float speed = distance / _navigationSelectTime;

        float scale = buttonTransform.localScale.x;
        while (buttonTransform.localScale.x > _navigationUnselectedScale)
        {
            scale = Mathf.Clamp(scale - Time.deltaTime * speed, _navigationUnselectedScale, 1);

            buttonTransform.localScale = new Vector2(scale, scale);
            yield return new WaitForFixedUpdate();
        }

        buttonTransform.localScale = new Vector2(_navigationUnselectedScale, _navigationUnselectedScale);

        yield return null;
    }

    #endregion

    void OnDestroy()
    {
        if (EarnManager.HasInstance)
        {
            EarnManager.Instance.OnSoftMoneyChange -= SoftMoneyChanged;
        }
    }
}
