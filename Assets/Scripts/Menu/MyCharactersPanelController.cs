﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class MyCharactersPanelController : CharactersPanelController
{
    #region Unity Editor Members

    //[SerializeField]
    //GridLayoutGroup _adquiredCharacters;

    //[SerializeField]
    //GridLayoutGroup _promotionCharacters;

    #endregion

    Dictionary<Character, Image> _cardsImages = new Dictionary<Character, Image>();

    void Start()
    {
        UpdateCharacters();

        CharacterCardController.OnCharacterChangeChange += CharacterChanged;
    }

    private void CharacterChanged(Character character)
    {
        UpdateCharacters();
    }

    public void UpdateCharacters()
    {
        foreach (Image cardsImage in _cardsImages.Values)
        {
            Destroy(cardsImage.transform.parent.gameObject);
        }

        _cardsImages.Clear();

        foreach (Character character in CharactersManager.Instance.PlayableCharacters.Values)
        {
            CharacterCardButton characterCardButton = Instantiate(_characterCardButtonPrefab);
            characterCardButton.Character = character;

            Image cardImage = characterCardButton.transform.FindChild("ThumbnailImage").GetComponent<Image>();
            cardImage.sprite = character.Model.ThumbnailImage;

            characterCardButton.GetComponent<Image>().color = GetElementColor(character.Model.Element);

            _cardsImages.Add(character, cardImage);

            //TODO; Only promoted!
            GridLayoutGroup parent = character.Owned ? _adquiredCharacters : _promotionCharacters;
            characterCardButton.transform.SetParent(parent.transform, false);
        }
    }
}
