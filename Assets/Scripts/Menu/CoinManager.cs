﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinManager : MonoBehaviour {

	[HideInInspector]
	public int coins;
	public Text coinText;

	void Start(){
		earnCoins();
	}

	public void earnCoins(){
		coins = EarnManager.Instance.Coins;
		earnTextCoins();
	}

	void earnTextCoins(){
		coinText.text = string.Format("x {0:#000}", coins);
	}
}
