﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class GUIManager : UIController<GUIManager> {

    private int scoreRed = 0;
    private int scoreBlue = 0;
    private int scoreYellow = 0;
    private int scoreGreen = 0;
    private int scoreHeart = 0;

    public Text scoreRedText;
	public Text scoreBlueText;
	public Text scoreYellowText;
	public Text scoreGreenText;
	public Text scoreHeartText;

    public int numCharInScene = 4;

    #region Unity Editor Members

    [Header("Victory UI")]
    [SerializeField]
    CanvasGroup _victoryGroup;

    [SerializeField]
    Text _coinsEarnedText;

    [SerializeField]
    Image _character1Image;
    [SerializeField]
    Image _character1DiedImage;
    [SerializeField]
    Slider _character1ExperienceSlider;

    [SerializeField]
    Image _character2Image;
    [SerializeField]
    Image _character2DiedImage;
    [SerializeField]
    Slider _character2ExperienceSlider;

    [SerializeField]
    Image _character3Image;
    [SerializeField]
    Image _character3DiedImage;
    [SerializeField]
    Slider _character3ExperienceSlider;

    [SerializeField]
    Image _character4Image;
    [SerializeField]
    Image _character4DiedImage;
    [SerializeField]
    Slider _character4ExperienceSlider;

    [SerializeField]
    Image _characterSocialImage;
    [SerializeField]
    Image _characterSocialDiedImage;
    [SerializeField]
    Slider _characterSocialExperienceSlider;

    [Header("Defeat UI")]
    [SerializeField]
    CanvasGroup _defeatedGroup;

    #endregion

    //#region Public properties

    //public Image Character1Image
    //{
    //    get
    //    {
    //        return _character1Image;
    //    }
    //}

    //public Slider Character1ExperienceSlider
    //{
    //    get
    //    {
    //        return _character1ExperienceSlider;
    //    }
    //}

    //public Image Character2Image
    //{
    //    get
    //    {
    //        return _character2Image;
    //    }
    //}

    //public Slider Character2ExperienceSlider
    //{
    //    get
    //    {
    //        return _character2ExperienceSlider;
    //    }
    //}

    //public Image Character3Image
    //{
    //    get
    //    {
    //        return _character3Image;
    //    }
    //}

    //public Slider Character3ExperienceSlider
    //{
    //    get
    //    {
    //        return _character3ExperienceSlider;
    //    }
    //}

    //public Image Character4Image
    //{
    //    get
    //    {
    //        return _character4Image;
    //    }
    //}

    //public Slider Character4ExperienceSlider
    //{
    //    get
    //    {
    //        return _character4ExperienceSlider;
    //    }
    //}

    //public Image CharacterSocialImage
    //{
    //    get
    //    {
    //        return _characterSocialImage;
    //    }
    //}

    //public Slider CharacterSocialExperienceSlider
    //{
    //    get
    //    {
    //        return _characterSocialExperienceSlider;
    //    }
    //}

    //#endregion


    /*void Update(){
		printScoreRed();
		printScoreBlue();
		printScoreYellow();
		printScoreGreen();
		printScoreLife();
	}*/

    public void AddScore(int AmountYellowToken, int AmountRedToken, int AmountGreenToken, int AmountBlueToken, int AmountHeartToken){
		scoreRed += AmountRedToken;
		scoreBlue += AmountBlueToken;
		scoreYellow += AmountYellowToken;
		scoreGreen += AmountGreenToken;
		scoreHeart += AmountHeartToken;

        //MLR
        if (AmountRedToken != 0) printScoreRed();
        if (AmountBlueToken != 0) printScoreBlue();
        if (AmountYellowToken != 0) printScoreYellow();
        if (AmountGreenToken != 0) printScoreGreen();
        if (AmountHeartToken != 0) printScoreLife();
    }

	void printScoreRed(){
		string scoreStr = scoreRed.ToString("00");

		scoreRedText.text = scoreStr;
	}

	void printScoreBlue(){
		string scoreStr = scoreBlue.ToString("00");

		scoreBlueText.text = scoreStr;
	}

	void printScoreYellow(){
		string scoreStr = scoreYellow.ToString("00");

		scoreYellowText.text = scoreStr;
	}

	void printScoreGreen(){
		string scoreStr = scoreGreen.ToString("00");

		scoreGreenText.text = scoreStr;
	}

	void printScoreLife(){
		string scoreStr = scoreHeart.ToString("00");

		scoreHeartText.text = scoreStr;
	}

    public void GoToLevelBriefing()
    {
        MenuUIController.MenuScreen = MenuUIController.Screen.LevelBriefing;
        ScenesManager.Instance.LoadScene(ScenesManager.MENU_SCENE);
    }

    public void RetryLevel()
    {
        ScenesManager.Instance.LoadScene(ScenesManager.GAME_SCENE);
    }

    public void ShowVictory()
    {
        SetVictoryValues();
        Animator _victoryAnimator = _victoryGroup.GetComponent<Animator>();
        //_victoryAnimator.SetInteger("Stars", MazController.Instance.stars);
        //TODO: Check experience obtained
        //TODO: Check chest reward
        _victoryGroup.gameObject.SetActive(true);
        _victoryAnimator.SetInteger("Stars", MazController.Instance.stars);
    }

    void ShowDefeated()
    {
        _defeatedGroup.gameObject.SetActive(true);
    }

	public void IsDefeat(){
		if(numCharInScene > 1){
			numCharInScene -= 1;
			print("YOU ARE NEAR TO DIE");
		}
		else if(numCharInScene == 1) {
			ShowDefeated();
			print("DIE POTATOE");
		}
	}

    public void NextAnimation(Animator animator)
    {
        AnimatorStateInfo animatorState = animator.GetCurrentAnimatorStateInfo(0);
        if (animatorState.length > 0 && animatorState.normalizedTime < 0.9f)
        {
            animator.Play(0, -1, 0.9f);
        }
    }

    void SetVictoryValues()
    {
        _coinsEarnedText.text = String.Format("+{0}", MazController.Instance.earnGold);

        SetVictoryCharacterValues(ManagerSelection.Instance.Character1, _character1DiedImage, _character1Image, _character1ExperienceSlider);
        SetVictoryCharacterValues(ManagerSelection.Instance.Character2, _character2DiedImage, _character2Image, _character2ExperienceSlider);
        SetVictoryCharacterValues(ManagerSelection.Instance.Character3, _character3DiedImage, _character3Image, _character3ExperienceSlider);
        SetVictoryCharacterValues(ManagerSelection.Instance.Character4, _character4DiedImage, _character4Image, _character4ExperienceSlider);
        SetVictoryCharacterValues(ManagerSelection.Instance.CharacterSocial, _characterSocialDiedImage, _characterSocialImage, _characterSocialExperienceSlider);
    }

    void SetVictoryCharacterValues(Character character, Image imageDied, Image image, Slider experienceSlider)
    {
        if (character != null)
        {
            image.sprite = character.Model.ThumbnailImage;
            if (character.ResurrectTime > Time.time)
            {
                imageDied.gameObject.SetActive(true);
            }

            if (character.CanLevelUp)
            {
                experienceSlider.maxValue = 1f;
                experienceSlider.value = 1f;
            }
            else
            {
                int nextLevel = character.Level + 1;
                float currentLevelExperience = CharactersManager.Instance.GetLevelExperience(character.Level);
                float nextLevelExperience = CharactersManager.Instance.GetLevelExperience(nextLevel);

                experienceSlider.maxValue = nextLevelExperience - currentLevelExperience;
                experienceSlider.value = experienceSlider.maxValue - character.ExperienceToNextLevel;
            }

        }
        else
        {
            image.gameObject.SetActive(false);
            experienceSlider.gameObject.SetActive(false);

            imageDied.gameObject.SetActive(true);
        }
    }
}
