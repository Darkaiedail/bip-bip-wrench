﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GemManager : Singleton<GemManager> {

	public GameObject gemTile;
	public Sprite[] TilePrefabs;
	private Collider2D[] neighbors;

	public bool isSelected = false;
	public bool isMatched = false;

	public int gemId;

	public bool MaxYellowAmount = false;
	public bool MaxRedAmount = false;
	public bool MaxGreenAmount = false;
	public bool MaxBlueAmount = false;
	public bool MaxHeartAmount = false;

	public bool gemIni = true;

	public GameObject[] gems;

	public static bool firstHit = false;

	private int pA; 

	private LineRenderer line;

	public GameObject[] characters;

	public int XCoord{
		get{
			return Mathf.RoundToInt(transform.localPosition.x);
		}
	}

	public int YCoord{
		get{
			return Mathf.RoundToInt(transform.localPosition.y);
		}
	}

	[HideInInspector]
	public bool doubleHability;

	void Awake(){
		createGemId();

		CreateGem();
	}

	void Start(){
		line = GetComponent<LineRenderer>();

		FindCharacters();
	}

	public void FindCharacters(){
		characters = GameObject.FindGameObjectsWithTag("Player");
	}
		
	public void ToggleSelector(){
		isSelected = !isSelected;
	}

	public void Deselect(){
		isSelected = false;
	}

	public void CreateGem(){
		if(gemIni){
			int randomTile = Random.Range(0,TilePrefabs.Length);

			if(randomTile == 0){
                BoardManager.Instance.YellowAmount ++;
			}

			if(randomTile == 1){
                BoardManager.Instance.RedAmount ++;
			}

			if(randomTile == 2){
                BoardManager.Instance.GreenAmount ++;
			}

			if(randomTile == 3){
                BoardManager.Instance.BlueAmount ++;
			}

			if(randomTile == 4){
                BoardManager.Instance.HeartAmount ++;
			}

			if(BoardManager.Instance.YellowAmount >= 8){
				randomTile = Random.Range(1,TilePrefabs.Length);
			}

			if(BoardManager.Instance.YellowAmount >= 8 &&
                    BoardManager.Instance.RedAmount >= 8){
				randomTile = Random.Range(2,TilePrefabs.Length);
			}
			if(BoardManager.Instance.YellowAmount >= 8 &&
                    BoardManager.Instance.RedAmount >= 8 &&
                    BoardManager.Instance.GreenAmount >= 8){
				randomTile = Random.Range(3,TilePrefabs.Length);
			}
			if(BoardManager.Instance.YellowAmount >= 8 &&
                    BoardManager.Instance.RedAmount >= 8 &&
                    BoardManager.Instance.GreenAmount >= 8 &&
                    BoardManager.Instance.BlueAmount >= 8){
				randomTile = 4;
			}

			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[randomTile];

			createGemId();

			isMatched = false;
		}

		else if(!gemIni){
			int randomTile = Random.Range(0,TilePrefabs.Length);

			if(randomTile == 0){
                BoardManager.Instance.YellowAmount ++;
			}

			if(randomTile == 1){
                BoardManager.Instance.RedAmount ++;
			}

			if(randomTile == 2){
                BoardManager.Instance.GreenAmount ++;
			}

			if(randomTile == 3){
                BoardManager.Instance.BlueAmount ++;
			}

			if(randomTile == 4){
                BoardManager.Instance.HeartAmount ++;
			}

			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[Random.Range(0,TilePrefabs.Length - 1)];

			createGemId();

			isMatched = false;
		}

	}

	private void createGemId(){
		
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[0])){
			gemId = 1;//Yellow gems
		}
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[1])){
			gemId = 2;//red gems
		}
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[2])){
			gemId = 3;//green gems
		}
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[3])){
			gemId = 4;//blue gems
		}
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[4])){
			gemId = 5;//life gems
		}
	}


	public void destroyYellow(){
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[1]) || Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[2]) ||
			Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[3]) || Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[4])){
			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[0];
		}

		createGemId();
	}

	public void destroyRed(){
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[0]) || Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[2]) ||
			Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[3]) || Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[4])){
			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[1];
		}

		createGemId();
	}

	public void destroyGreen(){
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[0]) || Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[1]) ||
			Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[3]) || Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[4])){
			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[2];
		}

		createGemId();
	}

	public void destroyBlue(){
		if(Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[0]) || Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[1]) ||
			Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[2]) || Resources.Equals(gemTile.GetComponent<SpriteRenderer>().sprite, TilePrefabs[4])){
			gemTile.GetComponent<SpriteRenderer>().sprite = TilePrefabs[3];
		}

		createGemId();
	}

     void OnMouseOver() {
        if (Input.GetMouseButton(0)){
            if (!isSelected){
                if (!firstHit){
					SelectGem();

                    firstHit = true;

					pA = 0;
                }

				else if (BoardManager.Instance.Neighbors[BoardManager.Instance.Neighbors.Count - 1].GetComponent<GemManager>().gemId == gemId){
					SelectGem();

					pA = 0;
                }

				else if (BoardManager.Instance.Neighbors[BoardManager.Instance.Neighbors.Count - 1].GetComponent<GemManager>().gemId != gemId && BoardManager.Instance.pA > 0){
					SelectGem();

					pA = 2;

                    BoardManager.Instance.pA -= 2;
                    BoardManager.Instance.printSustractPAs();
					MazController.Instance.pAAccumulated += 2;

                    BoardManager.Instance.substractPAs(2.0f);

					for (int i = 0; i < characters.Length; i++){
                        characters[i].GetComponent<CharController>().combo = true;
                    }
                }

				else if (BoardManager.Instance.Neighbors[BoardManager.Instance.Neighbors.Count - 1].GetComponent<GemManager>().gemId != gemId && BoardManager.Instance.pA <= 0){
                  
                }

                UpdateLines();
            }
            //MLR
            else
            {
                if (BoardManager.Instance.Neighbors.Count > 1 && BoardManager.Instance.Neighbors[BoardManager.Instance.Neighbors.Count - 2] == this)
                {
                    GemManager lastGem = BoardManager.Instance.Neighbors[BoardManager.Instance.Neighbors.Count - 1];
                    lastGem.Deselect();
                    lastGem.line.enabled = false;

					BoardManager.Instance.pA += lastGem.pA;
					BoardManager.Instance.substractPAs(-lastGem.pA);
					MazController.Instance.pAAccumulated -= lastGem.pA;

                    BoardManager.Instance.Neighbors.Remove(lastGem);

                    if (BoardManager.Instance.Neighbors.Count == 0)
                    {
                        firstHit = false;
                    }

                    UpdateLines();
                }
            }
        }
    }

	void SelectGem(){
		isSelected = true;
		BoardManager.Instance.Neighbors.Add(this);

		neighbors = Physics2D.OverlapCircleAll(this.transform.position,.4f, 1 << 8);
	}

    void UpdateLines(){
        for (int i = 0; i < BoardManager.Instance.Neighbors.Count; i++){
            int j = i + 1;

            BoardManager.Instance.Neighbors[i].GetComponent<GemManager>().line.enabled = true;
            Vector3 adding = new Vector3(0, 0, -2);

            GemManager thisGem = BoardManager.Instance.Neighbors[i];
            GemManager nextGem = j < BoardManager.Instance.Neighbors.Count ? BoardManager.Instance.Neighbors[j] : null; //MLR: Fixed null reference exception


            if (thisGem)
            {
                BoardManager.Instance.Neighbors[i].GetComponent<GemManager>().line.SetPosition(0, BoardManager.Instance.Neighbors[i].transform.position + adding);
                BoardManager.Instance.Neighbors[i].GetComponent<GemManager>().line.SetPosition(1, BoardManager.Instance.Neighbors[i].transform.position + adding);
            }

            if (nextGem != null)
            {
                BoardManager.Instance.Neighbors[i].GetComponent<GemManager>().line.SetPosition(0, BoardManager.Instance.Neighbors[i].transform.position + adding);
                BoardManager.Instance.Neighbors[i].GetComponent<GemManager>().line.SetPosition(1, BoardManager.Instance.Neighbors[j].transform.position + adding);
            }
        }
    }

	void Update(){
		gems = GameObject.FindGameObjectsWithTag("Gem");

		FindCharacters();

		if(Input.GetMouseButtonUp(0)) {
            BoardManager.Instance.CheckForNearbyMatches();

			for(int i = 0; i < gems.Length; i++){
				gems[i].GetComponent<GemManager>().Deselect();

				gems[i].GetComponent<GemManager>().line.enabled = false;

				gems[i].GetComponent<GemManager>().line.SetPosition(0, Vector3.zero);
				gems[i].GetComponent<GemManager>().line.SetPosition(1, Vector3.zero);

                BoardManager.Instance.Neighbors.Clear();
			}

            firstHit = false;

			if(doubleHability){
				HabilityController.Instance.numTurns += 1;
			}
        }
    }
}
