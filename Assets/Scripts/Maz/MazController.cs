﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MazController :  Singleton<MazController>  {

    [SerializeField]
    SpriteRenderer _battlerRendererPrefab;

	//public GameObject[] characters;
	public GameObject[] charPos;

	[HideInInspector]
	public int numOfRounds;
	[HideInInspector]
	public int[] numEnemRound;

	public GameObject[] enemies;
	public GameObject[] bosses;
	public GameObject[] positions;
	public GameObject bossPosition;

	[HideInInspector]
	public int numPosFirstEnemy;
	[HideInInspector]
	public int numPosLastEnemy;
	[HideInInspector]
	public int numPosBoss;

	public GameObject[] roundsText;
	public GameObject bossText;

	[HideInInspector]
	public GameObject[] sceneCharacters;
	[HideInInspector]
	public GameObject[] sceneEnemies;

	private bool nextRound;

	[HideInInspector]
	public int earnExp;
	[HideInInspector]
	public int earnGold;
	[HideInInspector]
	public int earnChest;

	[HideInInspector]
	public int exp;
	[HideInInspector]
	public int gold;
	[HideInInspector]
	public int chest;
	public Text coinText;
	public Text expText;
	public GameObject chestPanel;

	public GameObject panelFinal;

	private int pA;
	//[HideInInspector]
	public int pAAccumulated;
	[HideInInspector]
	public int maxPA;

	protected string currentLevel;
	protected int worldIndex;
	protected int levelIndex;

	[HideInInspector]
	public int stars = 0;
	public GameObject starsPanel;
	public GameObject firstStar;
	public GameObject secondStar;
	public GameObject thirdStar;

    public CharController char1Controller;
    public CharController char2Controller;
    public CharController char3Controller;
    public CharController char4Controller;
    public CharController charSocialController;


    void Awake(){
		WorldController.Instance.levelParameters();

		//GameObject manager = GameObject.Find("ManagerSelection");

        if (ManagerSelection.Instance.Character1 != null)
        {
            SpriteRenderer character1 = Instantiate(_battlerRendererPrefab, charPos[0].transform.position, Quaternion.identity) as SpriteRenderer;
            character1.sprite = ManagerSelection.Instance.Character1.Model.BattlerImage;

            char1Controller = character1.GetComponent<CharController>();
            HabilityController characterHabilityController = character1.GetComponent<HabilityController>();

            char1Controller.h = 0;
            char1Controller.health = ManagerSelection.Instance.Character1.BaseHealth;
            char1Controller.charDamage = ManagerSelection.Instance.Character1.BaseDamage;
            char1Controller.heartsValue = ManagerSelection.Instance.Character1.BaseReparation;
            char1Controller.taunt = ManagerSelection.Instance.Character1.BaseTaunt;
            char1Controller.charType = ManagerSelection.Instance.Character1.Model.Element;

            char1Controller.life = char1Controller.health;

            characterHabilityController.h = 0;
            characterHabilityController.charHability = ManagerSelection.Instance.Character1.Model.Hability;
            characterHabilityController.tokensToHability = ManagerSelection.Instance.Character1.Model.TokensToHability;
            characterHabilityController.timeToRestartEnemyStats = ManagerSelection.Instance.Character1.Model.TimeToRestartEnemyStats;
            characterHabilityController.selectorPrefab = ManagerSelection.Instance.Character1.Model.HabilityReadyParticleSystem;



        }

        if (ManagerSelection.Instance.Character2 != null)
        {
            SpriteRenderer character2 = Instantiate(_battlerRendererPrefab, charPos[1].transform.position, Quaternion.identity) as SpriteRenderer;
            character2.sprite = ManagerSelection.Instance.Character2.Model.BattlerImage;

            char2Controller = character2.GetComponent<CharController>();
            HabilityController characterHabilityController = character2.GetComponent<HabilityController>();

            char2Controller.h = 1;
            char2Controller.health = ManagerSelection.Instance.Character2.BaseHealth;
            char2Controller.charDamage = ManagerSelection.Instance.Character2.BaseDamage;
            char2Controller.heartsValue = ManagerSelection.Instance.Character2.BaseReparation;
            char2Controller.taunt = ManagerSelection.Instance.Character2.BaseTaunt;
            char2Controller.charType = ManagerSelection.Instance.Character2.Model.Element;

            char2Controller.life = char2Controller.health;

            characterHabilityController.h = 1;
            characterHabilityController.charHability = ManagerSelection.Instance.Character2.Model.Hability;
            characterHabilityController.tokensToHability = ManagerSelection.Instance.Character2.Model.TokensToHability;
            characterHabilityController.timeToRestartEnemyStats = ManagerSelection.Instance.Character2.Model.TimeToRestartEnemyStats;
            characterHabilityController.selectorPrefab = ManagerSelection.Instance.Character2.Model.HabilityReadyParticleSystem;
        }

        if (ManagerSelection.Instance.Character3 != null)
        {
            SpriteRenderer character3 = Instantiate(_battlerRendererPrefab, charPos[2].transform.position, Quaternion.identity) as SpriteRenderer;
            character3.sprite = ManagerSelection.Instance.Character3.Model.BattlerImage;

            char3Controller = character3.GetComponent<CharController>();
            HabilityController characterHabilityController = character3.GetComponent<HabilityController>();

            char3Controller.h = 2;
            char3Controller.health = ManagerSelection.Instance.Character3.BaseHealth;
            char3Controller.charDamage = ManagerSelection.Instance.Character3.BaseDamage;
            char3Controller.heartsValue = ManagerSelection.Instance.Character3.BaseReparation;
            char3Controller.taunt = ManagerSelection.Instance.Character3.BaseTaunt;
            char3Controller.charType = ManagerSelection.Instance.Character3.Model.Element;

            char3Controller.life = char3Controller.health;

            characterHabilityController.h = 2;
            characterHabilityController.charHability = ManagerSelection.Instance.Character3.Model.Hability;
            characterHabilityController.tokensToHability = ManagerSelection.Instance.Character3.Model.TokensToHability;
            characterHabilityController.timeToRestartEnemyStats = ManagerSelection.Instance.Character3.Model.TimeToRestartEnemyStats;
            characterHabilityController.selectorPrefab = ManagerSelection.Instance.Character3.Model.HabilityReadyParticleSystem;
        }

        if (ManagerSelection.Instance.Character4 != null) {
            SpriteRenderer character4 = Instantiate(_battlerRendererPrefab, charPos[3].transform.position, Quaternion.identity) as SpriteRenderer;
            character4.sprite = ManagerSelection.Instance.Character4.Model.BattlerImage;

            char4Controller = character4.GetComponent<CharController>();
            HabilityController characterHabilityController = character4.GetComponent<HabilityController>();

            char4Controller.h = 3;
            char4Controller.health = ManagerSelection.Instance.Character4.BaseHealth;
            char4Controller.charDamage = ManagerSelection.Instance.Character4.BaseDamage;
            char4Controller.heartsValue = ManagerSelection.Instance.Character4.BaseReparation;
            char4Controller.taunt = ManagerSelection.Instance.Character4.BaseTaunt;
            char4Controller.charType = ManagerSelection.Instance.Character4.Model.Element;

            char4Controller.life = char4Controller.health;

            characterHabilityController.h = 3;
            characterHabilityController.charHability = ManagerSelection.Instance.Character4.Model.Hability;
            characterHabilityController.tokensToHability = ManagerSelection.Instance.Character4.Model.TokensToHability;
            characterHabilityController.timeToRestartEnemyStats = ManagerSelection.Instance.Character4.Model.TimeToRestartEnemyStats;
            characterHabilityController.selectorPrefab = ManagerSelection.Instance.Character4.Model.HabilityReadyParticleSystem;
        }

        if (ManagerSelection.Instance.CharacterSocial != null)
        {
            SpriteRenderer characterSocial = Instantiate(_battlerRendererPrefab, charPos[4].transform.position, Quaternion.identity) as SpriteRenderer;
            characterSocial.sprite = ManagerSelection.Instance.CharacterSocial.Model.BattlerImage;

            charSocialController = characterSocial.GetComponent<CharController>();
            HabilityController characterHabilityController = characterSocial.GetComponent<HabilityController>();

            charSocialController.h = 4;
            charSocialController.health = ManagerSelection.Instance.CharacterSocial.BaseHealth;
            charSocialController.charDamage = ManagerSelection.Instance.CharacterSocial.BaseDamage;
            charSocialController.heartsValue = ManagerSelection.Instance.CharacterSocial.BaseReparation;
            charSocialController.taunt = ManagerSelection.Instance.CharacterSocial.BaseTaunt;
            charSocialController.charType = ManagerSelection.Instance.CharacterSocial.Model.Element;

            charSocialController.life = charSocialController.health;

            characterHabilityController.h = 4;
            characterHabilityController.charHability = ManagerSelection.Instance.CharacterSocial.Model.Hability;
            characterHabilityController.tokensToHability = ManagerSelection.Instance.CharacterSocial.Model.TokensToHability;
            characterHabilityController.timeToRestartEnemyStats = ManagerSelection.Instance.CharacterSocial.Model.TimeToRestartEnemyStats;
        }

        /*if (manager.GetComponent<ManagerSelection>().i != 5){
			GameObject charOne = Instantiate(characters[manager.GetComponent<ManagerSelection>().i], charPos[0].transform.position, Quaternion.identity) as GameObject;

			charOne.GetComponent<HabilityController>().h = 0;
			charOne.GetComponent<CharController>().h = 0;
		}
		if(manager.GetComponent<ManagerSelection>().j != 5){
			GameObject charTwo = Instantiate(characters[manager.GetComponent<ManagerSelection>().j], charPos[1].transform.position, Quaternion.identity) as GameObject;

			charTwo.GetComponent<HabilityController>().h = 1;
			charTwo.GetComponent<CharController>().h = 1;
		}
		if(manager.GetComponent<ManagerSelection>().k != 5){
			GameObject charThree = Instantiate(characters[manager.GetComponent<ManagerSelection>().k], charPos[2].transform.position, Quaternion.identity) as GameObject;

			charThree.GetComponent<HabilityController>().h = 2;
			charThree.GetComponent<CharController>().h = 2;
		}
		if(manager.GetComponent<ManagerSelection>().h != 5){
			GameObject charFour = Instantiate(characters[manager.GetComponent<ManagerSelection>().h], charPos[3].transform.position, Quaternion.identity) as GameObject;

			charFour.GetComponent<HabilityController>().h = 3;
			charFour.GetComponent<CharController>().h = 3;
		}*/
    }

	void Start(){
		pA = BoardManager.Instance.pA;

		roundsText[numOfRounds].SetActive(true);
		StartCoroutine("desactiveText");

		sceneCharacters = GameObject.FindGameObjectsWithTag("Player");

		StartCoroutine("InstantiateFirstEnemies");

		for(int i = 0; i < sceneCharacters.Length; i++){
			sceneCharacters[i].GetComponent<CharController>().firstRound = true;
		}

        BoardManager.Instance.firstRound = true;

		numOfRounds -= 1;

		currentLevel = EarnManager.Instance.SceneLevel;
	}

	IEnumerator InstantiateFirstEnemies(){
		yield return new WaitForSeconds(.6f);

		for(int i = 0; i < numEnemRound[0]; i++){
			Instantiate(enemies[Random.Range(0,enemies.Length)], positions[i].transform.position, Quaternion.identity);
		}

		sceneEnemies = GameObject.FindGameObjectsWithTag("Enemy");

		for(int i = 0; i < sceneCharacters.Length; i++){
			sceneCharacters[i].GetComponent<CharController>().selectRandomEnemy();
		}
	}

	void Update () {
		sceneCharacters = GameObject.FindGameObjectsWithTag("Player");

		if(nextRound){
			nextRound = false;

			numOfRounds -= 1;

			if(numOfRounds > 0){
				roundsText[numOfRounds].SetActive(true);

				StartCoroutine("InstantiateEnemies");
				StartCoroutine("desactiveText");
			}

			else if(numOfRounds == 0){
				bossText.SetActive(true);

				StartCoroutine("InstantiateBoss");
				StartCoroutine("desactiveText");
			}

			else if(numOfRounds < 0){
                bool winAllAliveStar = true;
				for(int i = 0; i < sceneCharacters.Length; i++){
                    if (sceneCharacters[i].transform == null)
                    {
                        winAllAliveStar = false;
                    }

					sceneCharacters[i].GetComponent<CharController>().mazEnd = true;

                    if (ManagerSelection.Instance.Character1 != null)
                    {
                        bool died = char1Controller.health <= 0;
                        if (died)
                        {
                            ManagerSelection.Instance.Character1.ResurrectTime = Time.time + 3600f;
                        }
                        else
                        {
                            ManagerSelection.Instance.Character1.TotalExperience += earnExp;
                            ManagerSelection.Instance.Character1.ResurrectTime = 0;
                        }
                    }
                    if (ManagerSelection.Instance.Character2 != null)
                    {
                        bool died = char2Controller.health <= 0;
                        if (died)
                        {
                            ManagerSelection.Instance.Character2.ResurrectTime = Time.time + 3600f;
                        }
                        else
                        {
                            ManagerSelection.Instance.Character2.TotalExperience += earnExp;
                            ManagerSelection.Instance.Character2.ResurrectTime = 0;
                        }
                    }
                    if (ManagerSelection.Instance.Character3 != null)
                    {
                        bool died = char3Controller.health <= 0;
                        if (died)
                        {
                            ManagerSelection.Instance.Character3.ResurrectTime = Time.time + 3600f;
                        }
                        else
                        {
                            ManagerSelection.Instance.Character3.TotalExperience += earnExp;
                            ManagerSelection.Instance.Character3.ResurrectTime = 0;
                        }
                    }
                    if (ManagerSelection.Instance.Character4 != null)
                    {
                        bool died = char4Controller.health <= 0;
                        if (died)
                        {
                            ManagerSelection.Instance.Character4.ResurrectTime = Time.time + 3600f;
                        }
                        else
                        {
                            ManagerSelection.Instance.Character4.TotalExperience += earnExp;
                            ManagerSelection.Instance.Character4.ResurrectTime = 0;
                        }
                    }
                    if (ManagerSelection.Instance.CharacterSocial != null)
                    {
                        bool died = charSocialController.health <= 0;
                        if (died)
                        {
                            ManagerSelection.Instance.CharacterSocial.ResurrectTime = Time.time + 3600f;
                        }
                        else
                        {
                            ManagerSelection.Instance.CharacterSocial.TotalExperience += (int)(earnExp * 0.5f);
                            ManagerSelection.Instance.CharacterSocial.ResurrectTime = 0;
                        }
                    }

                    /*if(sceneCharacters[i].GetComponent<LevelController>().charName == LevelController.TypeOfChar.RayMimi){
                        EarnManager.Instance.expRayMimi += earnExp;
					}
					if(sceneCharacters[i].GetComponent<LevelController>().charName == LevelController.TypeOfChar.FireMimi){
                        EarnManager.Instance.expFireMimi += earnExp;
					}
					if(sceneCharacters[i].GetComponent<LevelController>().charName == LevelController.TypeOfChar.PlantMimi){
                        EarnManager.Instance.expPlantMimi += earnExp;
					}
					if(sceneCharacters[i].GetComponent<LevelController>().charName == LevelController.TypeOfChar.WaterMimi){
                        EarnManager.Instance.expWaterMimi += earnExp;
					}
					if(sceneCharacters[i].GetComponent<LevelController>().charName == LevelController.TypeOfChar.SpecialMimi){
                        EarnManager.Instance.expSpecialMimi += earnExp;
					}*/

                    /*if(sceneCharacters[i].GetComponent<CharController>().health >= 0){
						stars += 1;
					}*/
                }

                exp += earnExp;
                gold += earnGold;
                chest += earnChest;
                stars += 1;

                if (winAllAliveStar) {
                    stars += 1;
                }

                //TODO: Check different objectives

                /*MLR: Spent less than maxPa
                if(pAAccumulated <= maxPA){
					stars += 1;
				}*/

                //MLR: Dont spent PA
				if (pAAccumulated == 0)
                {
                    stars += 1;
                }

                EarnManager.Instance.UpdateCharacters();
                EarnManager.Instance.Coins += earnGold;

                //TODO: Check last level of last world
                EarnManager.Instance.SceneLevel = UnlockLevels();

                SaveManager.Instance.Save();

                GUIManager.Instance.ShowVictory();

                /* MLR: Now is GUIManager.Instance.ShowVictory();
                panelFinal.SetActive(true);
				printCoins();
				printExp();
				print("You Win"); */
            }
        }
	}

	IEnumerator InstantiateEnemies(){
		yield return new WaitForSeconds(1.0f);

		for(int i = 0; i < numEnemRound[numOfRounds]; i++){
			Instantiate(enemies[Random.Range(0,enemies.Length)], positions[i].transform.position, Quaternion.identity);
		}

		BoardManager.Instance.pA = pA - 2;
		BoardManager.Instance.initialPAs = 14;
		BoardManager.Instance.restOfPAs = 14.0f;
		BoardManager.Instance.substractPAs(0f);

		sceneEnemies = GameObject.FindGameObjectsWithTag("Enemy");

		for(int i = 0; i < sceneCharacters.Length; i++){
			sceneCharacters[i].GetComponent<CharController>().selectRandomEnemy();
		}
	}

	IEnumerator InstantiateBoss(){
		yield return new WaitForSeconds(1.0f);

		Instantiate(bosses[Random.Range(0,bosses.Length)], bossPosition.transform.position, Quaternion.identity);

		BoardManager.Instance.pA = pA - 2;
		BoardManager.Instance.initialPAs = 14;
		BoardManager.Instance.restOfPAs = 14.0f;
		BoardManager.Instance.substractPAs(0f);

		sceneEnemies = GameObject.FindGameObjectsWithTag("Enemy");

		for(int i = 0; i < sceneCharacters.Length; i++){
			sceneCharacters[i].GetComponent<CharController>().selectRandomEnemy();
		}
	}

	public void nextRoundTrue(){
		nextRound = true;
		print("caca");
	}

	IEnumerator desactiveText(){
		yield return new WaitForSeconds(0.5f);

		for(int i = 0; i < roundsText.Length; i++){
			roundsText[i].SetActive(false);
		}

		bossText.SetActive(false);
	}

    /* MLR: Now is GUIManager.Instance.ShowVictory();
	void printCoins(){
		string coinsStr = gold.ToString("000");

		coinText.text = coinsStr;
	}

	void printExp(){
		string coinsStr = exp.ToString("000");

		expText.text = coinsStr;
	}

    
    public void winStars(){
		panelFinal.SetActive(false);
		starsPanel.SetActive(true);

		if(stars == 1){
			firstStar.SetActive(true);
			iTween.ScaleTo(firstStar.gameObject, iTween.Hash("y", 2f,"x", 2f,
				"time", .5f, "easetype","easeOutQuint"));
		}

		if(stars == 2){
			firstStar.SetActive(true);
			iTween.ScaleTo(firstStar.gameObject, iTween.Hash("y", 2f,"x", 2f,
				"time", .5f, "easetype","easeOutQuint"));

			StartCoroutine("activateSecondStar");
		}

		if(stars == 6){
			firstStar.SetActive(true);
			iTween.ScaleTo(firstStar.gameObject, iTween.Hash("y", 2f,"x", 2f,
				"time", .5f, "easetype","easeOutQuint"));
			
			StartCoroutine("activateSecondStar");
			
			StartCoroutine("activateThirdStar");
		}
	}

	IEnumerator activateSecondStar(){
		yield return new WaitForSeconds(.5f);

		secondStar.SetActive(true);
		iTween.ScaleTo(secondStar.gameObject, iTween.Hash("y", 2f,"x", 2f,
			"time", .5f, "easetype","easeOutQuint"));
	}

	IEnumerator activateThirdStar(){
		yield return new WaitForSeconds(1.0f);

		thirdStar.SetActive(true);
		iTween.ScaleTo(thirdStar.gameObject, iTween.Hash("y", 2f,"x", 2f,
			"time", .5f, "easetype","easeOutQuint"));
	}

    public void backMenu(){
		starsPanel.SetActive(false);

		if(stars >= 6){
			if(chest > 0){
				chestPanel.SetActive(true);
			}

			if(chest == 0){
                //SceneManager.LoadScene(0);
                ScenesManager.Instance.LoadScene(ScenesManager.MENU_SCENE);

                //GameObject.Find("Manager").GetComponent<EarnManager>().nextLevel = 0;
                //GameObject.Find("Manager").GetComponent<EarnManager>().nextLevel = GameObject.Find("Manager").GetComponent<EarnManager>().sceneLevel + 1 - 3;

                UnlockLevels();

				GameObject.Find("Manager").GetComponent<SaveManager>().Save();
			}
		}

		else {
            //SceneManager.LoadScene(0);
            ScenesManager.Instance.LoadScene(ScenesManager.MENU_SCENE);

            UnlockLevels();

			GameObject.Find("Manager").GetComponent<SaveManager>().Save();
		}
	}*/

	public void recieveChest(){
		if(chest > 0){
			GameObject.Find("GameManager").GetComponent<ChestController>().recieveReward();
			chest -= 1;
		}
	}

	protected string  UnlockLevels (){
		//set the playerprefs value of next level to 1 to unlock
		for(int i = 1; i <= LockLevel.worlds; i++){
			for(int j = 1; j <= LockLevel.levels; j++){
                if (currentLevel == i.ToString() + "-" + j.ToString())
                {
                    bool nextWorld = j == LockLevel.levels;
                    if (nextWorld && i == LockLevel.worlds)
                    {
                        //Last level of last world
                        return currentLevel;
                    }

                    worldIndex = nextWorld ? i + 1 : i;
					levelIndex  = nextWorld ? 1 : (j + 1);

                    string unlockedLevel = worldIndex.ToString() + "-" + levelIndex.ToString();

                    PlayerPrefs.SetInt("level_" + unlockedLevel, 1);
                    return unlockedLevel;
                }
			}
		}
        return currentLevel;
        //load the World1 level 
        //SceneManager.LoadScene("Menu");
        //ScenesManager.Instance.LoadScene(ScenesManager.MENU_SCENE);
    }
}
