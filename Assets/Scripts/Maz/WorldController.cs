﻿using UnityEngine;
using System.Collections;

public class WorldController : Manager<WorldController> {

	public void levelParameters(){
		switch(EarnManager.Instance.SceneLevel){

		case "1-1":

			MazController.Instance.numOfRounds = 3;
			MazController.Instance.numEnemRound[0] = 3;
			MazController.Instance.numEnemRound[1] = 4;

			//Posiciones en el array para hacer el random entre esos enemigos
			MazController.Instance.numPosFirstEnemy = 0;
			MazController.Instance.numPosLastEnemy = 4;
			MazController.Instance.numPosBoss = 0;

			MazController.Instance.earnExp = 200;
			MazController.Instance.earnGold = 100;
			MazController.Instance.chest = 0;
			MazController.Instance.maxPA = 40;

			ChestController.Instance.coins = 0;
			ChestController.Instance.screws = 0;
			ChestController.Instance.nuts = 0;

			break;

		case "1-2":

			MazController.Instance.numOfRounds = 4;
			MazController.Instance.numEnemRound[0] = 3;
			MazController.Instance.numEnemRound[1] = 2;
			MazController.Instance.numEnemRound[2] = 4;

			//Posiciones en el array para hacer el random entre esos enemigos
			MazController.Instance.numPosFirstEnemy = 0;
			MazController.Instance.numPosLastEnemy = 4;
			MazController.Instance.numPosBoss = 0;

			MazController.Instance.earnExp = 200;
			MazController.Instance.earnGold = 100;
			MazController.Instance.chest = 1;
			MazController.Instance.maxPA = 40;

			ChestController.Instance.coins = 100;
			ChestController.Instance.screws = 2;
			ChestController.Instance.nuts = 1;

			break;

		case "1-3":

			MazController.Instance.numOfRounds = 4;
			MazController.Instance.numEnemRound[0] = 3;
			MazController.Instance.numEnemRound[1] = 4;
			MazController.Instance.numEnemRound[2] = 4;

			//Posiciones en el array para hacer el random entre esos enemigos
			MazController.Instance.numPosFirstEnemy = 0;
			MazController.Instance.numPosLastEnemy = 4;
			MazController.Instance.numPosBoss = 0;

			MazController.Instance.earnExp = 200;
			MazController.Instance.earnGold = 150;
			MazController.Instance.chest = 1;
			MazController.Instance.maxPA = 40;

			ChestController.Instance.coins = 100;
			ChestController.Instance.screws = 3;
			ChestController.Instance.nuts = 1;

			break;

		case "1-4":

			MazController.Instance.numOfRounds = 5;
			MazController.Instance.numEnemRound[0] = 3;
			MazController.Instance.numEnemRound[1] = 4;
			MazController.Instance.numEnemRound[2] = 4;
			MazController.Instance.numEnemRound[3] = 3;

			//Posiciones en el array para hacer el random entre esos enemigos
			MazController.Instance.numPosFirstEnemy = 0;
			MazController.Instance.numPosLastEnemy = 4;
			MazController.Instance.numPosBoss = 0;

			MazController.Instance.earnExp = 210;
			MazController.Instance.earnGold = 170;
			MazController.Instance.chest = 1;
			MazController.Instance.maxPA = 40;

			ChestController.Instance.coins = 100;
			ChestController.Instance.screws = 4;
			ChestController.Instance.nuts = 1;

			break;

		case "1-5":

			MazController.Instance.numOfRounds = 6;
			MazController.Instance.numEnemRound[0] = 3;
			MazController.Instance.numEnemRound[1] = 2;
			MazController.Instance.numEnemRound[2] = 4;
			MazController.Instance.numEnemRound[3] = 3;
			MazController.Instance.numEnemRound[4] = 3;

			//Posiciones en el array para hacer el random entre esos enemigos
			MazController.Instance.numPosFirstEnemy = 0;
			MazController.Instance.numPosLastEnemy = 4;
			MazController.Instance.numPosBoss = 0;

			MazController.Instance.earnExp = 240;
			MazController.Instance.earnGold = 200;
			MazController.Instance.chest = 1;
			MazController.Instance.maxPA = 40;

			ChestController.Instance.coins = 100;
			ChestController.Instance.screws = 5;
			ChestController.Instance.nuts = 2;

			break;

		case "1-6":

			MazController.Instance.numOfRounds = 6;
			MazController.Instance.numEnemRound[0] = 2;
			MazController.Instance.numEnemRound[1] = 4;
			MazController.Instance.numEnemRound[2] = 4;
			MazController.Instance.numEnemRound[3] = 2;
			MazController.Instance.numEnemRound[4] = 3;

			//Posiciones en el array para hacer el random entre esos enemigos
			MazController.Instance.numPosFirstEnemy = 0;
			MazController.Instance.numPosLastEnemy = 4;
			MazController.Instance.numPosBoss = 0;

			MazController.Instance.earnExp = 245;
			MazController.Instance.earnGold = 205;
			MazController.Instance.chest = 1;
			MazController.Instance.maxPA = 40;

			ChestController.Instance.coins = 150;
			ChestController.Instance.screws = 5;
			ChestController.Instance.nuts = 2;

			break;

		case "1-7":

			MazController.Instance.numOfRounds = 6;
			MazController.Instance.numEnemRound[0] = 2;
			MazController.Instance.numEnemRound[1] = 2;
			MazController.Instance.numEnemRound[2] = 3;
			MazController.Instance.numEnemRound[3] = 3;
			MazController.Instance.numEnemRound[4] = 3;

			//Posiciones en el array para hacer el random entre esos enemigos
			MazController.Instance.numPosFirstEnemy = 0;
			MazController.Instance.numPosLastEnemy = 4;
			MazController.Instance.numPosBoss = 0;

			MazController.Instance.earnExp = 250;
			MazController.Instance.earnGold = 220;
			MazController.Instance.chest = 1;
			MazController.Instance.maxPA = 40;

			ChestController.Instance.coins = 200;
			ChestController.Instance.screws = 5;
			ChestController.Instance.nuts = 3;

			break;

		case "1-8":

			MazController.Instance.numOfRounds = 6;
			MazController.Instance.numEnemRound[0] = 3;
			MazController.Instance.numEnemRound[1] = 2;
			MazController.Instance.numEnemRound[2] = 3;
			MazController.Instance.numEnemRound[3] = 4;
			MazController.Instance.numEnemRound[4] = 3;

			//Posiciones en el array para hacer el random entre esos enemigos
			MazController.Instance.numPosFirstEnemy = 0;
			MazController.Instance.numPosLastEnemy = 4;
			MazController.Instance.numPosBoss = 0;

			MazController.Instance.earnExp = 250;
			MazController.Instance.earnGold = 250;
			MazController.Instance.chest = 1;
			MazController.Instance.maxPA = 40;

			ChestController.Instance.coins = 200;
			ChestController.Instance.screws = 5;
			ChestController.Instance.nuts = 3;

			break;

		case "1-9":

			MazController.Instance.numOfRounds = 6;
			MazController.Instance.numEnemRound[0] = 4;
			MazController.Instance.numEnemRound[1] = 4;
			MazController.Instance.numEnemRound[2] = 4;
			MazController.Instance.numEnemRound[3] = 4;
			MazController.Instance.numEnemRound[4] = 4;

			//Posiciones en el array para hacer el random entre esos enemigos
			MazController.Instance.numPosFirstEnemy = 0;
			MazController.Instance.numPosLastEnemy = 4;
			MazController.Instance.numPosBoss = 0;

			MazController.Instance.earnExp = 260;
			MazController.Instance.earnGold = 300;
			MazController.Instance.chest = 1;
			MazController.Instance.maxPA = 40;

			ChestController.Instance.coins = 250;
			ChestController.Instance.screws = 6;
			ChestController.Instance.nuts = 4;

			break;

		case "1-10":

			MazController.Instance.numOfRounds = 2;
			MazController.Instance.numEnemRound[0] = 0;

			//Posiciones en el array para hacer el random entre esos enemigos
			MazController.Instance.numPosFirstEnemy = 0;
			MazController.Instance.numPosLastEnemy = 4;
			MazController.Instance.numPosBoss = 0;

			MazController.Instance.earnExp = 350;
			MazController.Instance.earnGold = 500;
			MazController.Instance.chest = 1;
			MazController.Instance.maxPA = 40;

			ChestController.Instance.coins = 250;
			ChestController.Instance.screws = 6;
			ChestController.Instance.nuts = 4;

			break;
		}
	}
}
