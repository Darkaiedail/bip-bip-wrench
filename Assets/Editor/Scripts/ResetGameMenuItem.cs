﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

public class ResetGameMenuItem {

	[MenuItem("Bip-Bip Wrench/Reset Game")]
	static void DeleteAllData() {
		PlayerPrefs.DeleteAll();
		File.Delete(Application.persistentDataPath + "/playerInfo.dat");
	}
}
