﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(ScrollRect))]
public class UIScrollRectSnap : MonoBehaviour
{
    [SerializeField]
    GridLayoutGroup _gridLayoutGroup;
    [SerializeField]
    int _index = 0;
    [SerializeField]
    bool _lockByIndex = false;
    [SerializeField]
    float minSpeed = 80.0f;
    [SerializeField]
    float _snapSpeed = 8.0f;

    ScrollRect _scrollRect;
    Vector2 _target = Vector2.zero;

    void Start()
    {
        _scrollRect = this.GetComponent<ScrollRect>(); // Cache the scroll rect
        Index(0); // Set the starting view element to the first one.
        _scrollRect.normalizedPosition = this.NormalizedPosition * this._index; // Set the normalization
    }

    void Update()
    {
        // Clamp by getting changes in the index.
        if (this._lockByIndex == true)
        {
            Vector2 target = this.NormalizedPosition * this._index;

            if (this._scrollRect.normalizedPosition != target)
                this._scrollRect.normalizedPosition = Vector2.Lerp(this._scrollRect.normalizedPosition, target, this._snapSpeed * Time.deltaTime);
        }
        else {
            if (this._scrollRect.velocity.magnitude <= this.minSpeed)
            {
                if (this._scrollRect.normalizedPosition != _target)
                    this._scrollRect.normalizedPosition = Vector2.Lerp(this._scrollRect.normalizedPosition, this._target, this._snapSpeed * Time.deltaTime);
            }
        }
    }

    // The size for a single cell (element)
    Vector2 SingleCell
    {
        get
        {
            return new Vector2(this._gridLayoutGroup.cellSize.x + this._gridLayoutGroup.spacing.x, this._gridLayoutGroup.cellSize.y + this._gridLayoutGroup.spacing.y);
        }
    }

    // The dimensions of the ScrollRect
    Vector2 ScrollRectDimension
    {
        get
        {
            return new Vector2(this._scrollRect.GetComponent<RectTransform>().rect.width, this._scrollRect.GetComponent<RectTransform>().rect.height);
        }
    }

    // The total size of the elements in the X and Y
    Vector2 ElementSize
    {
        get
        {
            return new Vector2(this._gridLayoutGroup.cellSize.x * (float)this._gridLayoutGroup.transform.childCount, this._gridLayoutGroup.cellSize.y * (float)this._gridLayoutGroup.transform.childCount);
        }
    }

    // The delta of both the X an Y the ScrollView RecTransfrom understands.
    Vector2 TotalDelta
    {
        get
        {
            return new Vector2(this.ElementSize.x + this._gridLayoutGroup.padding.left + this._gridLayoutGroup.padding.right + (float)(this._gridLayoutGroup.transform.childCount - 1) * this._gridLayoutGroup.spacing.x, this.ElementSize.y + this._gridLayoutGroup.padding.top + this._gridLayoutGroup.padding.bottom + (float)(this._gridLayoutGroup.transform.childCount - 1) * this._gridLayoutGroup.spacing.y) - this.ScrollRectDimension;
        }
    }

    // The position of the element normalized
    Vector2 NormalizedPosition
    {
        get
        {
            return new Vector2(this.SingleCell.x / this.TotalDelta.x, this.SingleCell.y / this.TotalDelta.y);
        }
    }

    int Elements
    {
        get
        {
            // How many elements in the scroll view GridLayout.
            return this._gridLayoutGroup.transform.childCount;
        }
    }

    public void OnValueChanged(Vector2 normalized)
    {
        if (this._scrollRect.horizontal == true)
        {
            int elementIndex = 0;

            float distance = Mathf.Abs(this._scrollRect.normalizedPosition.x - (this.NormalizedPosition.x) * elementIndex);

            // Find the closest target to the current normalization
            for (int i = 0; i < this.Elements; i++)
            {
                float possibleDistance = Mathf.Abs(this._scrollRect.normalizedPosition.x - this.NormalizedPosition.x * i);

                if (possibleDistance < distance)
                {
                    elementIndex = i;
                    distance = possibleDistance;
                }
            }

            // View the element at.
            this.Index(elementIndex);

            // Set the target normalization to...
            this._target = this.NormalizedPosition * this._index;
        }

        if (this._scrollRect.vertical == true)
        {
            int elementIndex = 0;

            float distance = Mathf.Abs(this._scrollRect.normalizedPosition.y - (this.NormalizedPosition.y) * elementIndex);

            // Find the closest target to the current normalization
            for (int i = 0; i < this.Elements; i++)
            {
                float possibleDistance = Mathf.Abs(this._scrollRect.normalizedPosition.y - this.NormalizedPosition.y * i);

                if (possibleDistance < distance)
                {
                    elementIndex = i;
                    distance = possibleDistance;
                }
            }

            // View the element at...
            this.Index(elementIndex);

            // Set the target normalization to...
            this._target = this.NormalizedPosition * this._index;
        }
    }

    // Manually change the index element we should be looking at (great for GamePads)
    public void Index(int index)
    {
        this._index = index;

        this._index = Mathf.Clamp(this._index, 0, this.Elements);
    }

    // Lock the ScrollView by only snapping by changes in the Index (great for GamePads)
    public void LockByIndex(bool lockByIndex)
    {
        this._lockByIndex = lockByIndex;
    }
}
