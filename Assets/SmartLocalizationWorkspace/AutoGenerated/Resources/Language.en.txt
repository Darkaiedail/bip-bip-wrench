<?xml version="1.0" encoding="utf-8"?>
<root><!-- 
    Microsoft ResX Schema 
    
    Version 2.0
    
    The primary goals of this format is to allow a simple XML format 
    that is mostly human readable. The generation and parsing of the 
    various data types are done through the TypeConverter classes 
    associated with the data types.
    
    Example:
    
    ... ado.net/XML headers & schema ...
    <resheader name="resmimetype">text/microsoft-resx</resheader>
    <resheader name="version">2.0</resheader>
    <resheader name="reader">System.Resources.ResXResourceReader, System.Windows.Forms, ...</resheader>
    <resheader name="writer">System.Resources.ResXResourceWriter, System.Windows.Forms, ...</resheader>
    <data name="Name1"><value>this is my long string</value><comment>this is a comment</comment></data>
    <data name="Color1" type="System.Drawing.Color, System.Drawing">Blue</data>
    <data name="Bitmap1" mimetype="application/x-microsoft.net.object.binary.base64">
        <value>[base64 mime encoded serialized .NET Framework object]</value>
    </data>
    <data name="Icon1" type="System.Drawing.Icon, System.Drawing" mimetype="application/x-microsoft.net.object.bytearray.base64">
        <value>[base64 mime encoded string representing a byte array form of the .NET Framework object]</value>
        <comment>This is a comment</comment>
    </data>
                
    There are any number of "resheader" rows that contain simple 
    name/value pairs.
    
    Each data row contains a name, and value. The row also contains a 
    type or mimetype. Type corresponds to a .NET class that support 
    text/value conversion through the TypeConverter architecture. 
    Classes that don't support this are serialized and stored with the 
    mimetype set.
    
    The mimetype is used for serialized objects, and tells the 
    ResXResourceReader how to depersist the object. This is currently not 
    extensible. For a given mimetype the value must be set accordingly:
    
    Note - application/x-microsoft.net.object.binary.base64 is the format 
    that the ResXResourceWriter will generate, however the reader can 
    read any of the formats listed below.
    
    mimetype: application/x-microsoft.net.object.binary.base64
    value   : The object must be serialized with 
            : System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
            : and then encoded with base64 encoding.
    
    mimetype: application/x-microsoft.net.object.soap.base64
    value   : The object must be serialized with 
            : System.Runtime.Serialization.Formatters.Soap.SoapFormatter
            : and then encoded with base64 encoding.

    mimetype: application/x-microsoft.net.object.bytearray.base64
    value   : The object must be serialized into a byte array 
            : using a System.ComponentModel.TypeConverter
            : and then encoded with base64 encoding.
    -->
  <xsd:schema id="root" xmlns="" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
    <xsd:import namespace="http://www.w3.org/XML/1998/namespace" />
    <xsd:element name="root" msdata:IsDataSet="true">
      <xsd:complexType>
        <xsd:choice maxOccurs="unbounded">
          <xsd:element name="metadata">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" />
              </xsd:sequence>
              <xsd:attribute name="name" use="required" type="xsd:string" />
              <xsd:attribute name="type" type="xsd:string" />
              <xsd:attribute name="mimetype" type="xsd:string" />
              <xsd:attribute ref="xml:space" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="assembly">
            <xsd:complexType>
              <xsd:attribute name="alias" type="xsd:string" />
              <xsd:attribute name="name" type="xsd:string" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="data">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" msdata:Ordinal="1" />
                <xsd:element name="comment" type="xsd:string" minOccurs="0" msdata:Ordinal="2" />
              </xsd:sequence>
              <xsd:attribute name="name" type="xsd:string" use="required" msdata:Ordinal="1" />
              <xsd:attribute name="type" type="xsd:string" msdata:Ordinal="3" />
              <xsd:attribute name="mimetype" type="xsd:string" msdata:Ordinal="4" />
              <xsd:attribute ref="xml:space" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="resheader">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" msdata:Ordinal="1" />
              </xsd:sequence>
              <xsd:attribute name="name" type="xsd:string" use="required" />
            </xsd:complexType>
          </xsd:element>
        </xsd:choice>
      </xsd:complexType>
    </xsd:element>
  </xsd:schema>
  <resheader name="resmimetype">
    <value>text/microsoft-resx</value>
  </resheader>
  <resheader name="version">
    <value>2.0</value>
  </resheader>
  <resheader name="reader">
    <value>System.Resources.ResXResourceReader, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>
  </resheader>
  <resheader name="writer">
    <value>System.Resources.ResXResourceWriter, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>
  </resheader>
	<data name="coldbot_description" xml:space="preserve">
		<value>Cold-Bot is NOT cool. Cold-Bot is just COLD.</value>
	</data>
	<data name="coldbot_name" xml:space="preserve">
		<value>Cold-Bot</value>
	</data>
	<data name="Defeated_Title" xml:space="preserve">
		<value>You have been defeated...</value>
	</data>
	<data name="Exp" xml:space="preserve">
		<value>EXP: {0:#,##0} / {1:#,##0}</value>
	</data>
	<data name="flamescorpion_description" xml:space="preserve">
		<value>¡Fire!</value>
	</data>
	<data name="flamescorpion_name" xml:space="preserve">
		<value>Flame Scorpion</value>
	</data>
	<data name="illuminated_description" xml:space="preserve">
		<value>I have an idea...</value>
	</data>
	<data name="illuminated_name" xml:space="preserve">
		<value>Illuminated</value>
	</data>
	<data name="LevelBoss" xml:space="preserve">
		<value>Boss</value>
	</data>
	<data name="LevelTitle" xml:space="preserve">
		<value>Level {0}</value>
	</data>
	<data name="Loading" xml:space="preserve">
		<value>Loading...</value>
	</data>
	<data name="Lvl" xml:space="preserve">
		<value>Lvl. {0}</value>
	</data>
	<data name="mechroom_description" xml:space="preserve">
		<value>Is it a mech? Is it a mushroom? No! It's MECHROOM!!!!</value>
	</data>
	<data name="mechroom_name" xml:space="preserve">
		<value>Mechroom</value>
	</data>
	<data name="Objective_KillEnemies" xml:space="preserve">
		<value>Kill all the enemies</value>
	</data>
	<data name="Objective_NoPA" xml:space="preserve">
		<value>Do not spent PA</value>
	</data>
	<data name="Objective_TeamSurvive" xml:space="preserve">
		<value>All your team must survive</value>
	</data>
	<data name="seabrainy_description" xml:space="preserve">
		<value>Glu, glu, glu...</value>
	</data>
	<data name="seabrainy_name" xml:space="preserve">
		<value>Seabrainy</value>
	</data>
	<data name="Victory_Title" xml:space="preserve">
		<value>Victory!</value>
	</data>
	<data name="World01_Title" xml:space="preserve">
		<value>World 1</value>
	</data>
	<data name="World02_Title" xml:space="preserve">
		<value>&lt;color=yellow&gt;Stage 2:&lt;/color&gt;
Botsland</value>
	</data>
</root>